# Procedural Map Generation (C++11)

This project is a Voronoi-diagram-based random planet generator. The ultimate idea is to create a realistic world utilizing a variety of algorithms and techniques.

![](images/overview.png)
![](images/overview2.png)

## Setup / Usage

The project relies on glew, freeglut and glm libraries. It has been tested on Cremi's computers and Ubuntu 18.04.02 LTS.

```bash
cd GPCP
mkdir build
cd build
cmake ..
make
```

Once all these steps have been respected, you'll find the executable in the build directory. You can use the programm without lauching the graphical interface by adding the option "-nodisplay" to the following commands (if you are just interested in generating files).

```bash
./gpcp <Generatorfile.txt> # Command if you have a .txt file of generators
  OR
./gpcp <number>            # Command for a random generation
```
#### Right-Click to open the menu
![chair](images/menu.png)

#### Modiable parameters by the user (top-left corner)
![chair](images/parameters.png)

For the moment there is no way of changing the number of plates once the program is launched, still you can modify it by changing the value of NB_PLATES in gpcp.cc.

****

## Test / Debug

For debug and test purposes run :
```bash
cd GPCP
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug ..
make
```

To execute all the tests at once there's the custom command **make runTests**. All tests have been implemented with the googletest framework.

To run our program with gprof you can type :
```bash
cd GPCP
mkdir build
cd build
cmake -DCMAKE_CXX_FLAGS=-pg ..
make
make profile		        # With graphical interface
	# OR
make profile-no-display	 # Without graphical interface
```

## CONTRIBUTORS

[Zhengde Qiu](@zhengde),
[Simon Tricoire](@stricoire),
[Adrien Veschambre](@aveschambre),
[Gabriel Weil](@gweil)
