// "Copyright 2019 Billy Boys"
#ifndef GPCP_INCLUDE_REGION_H_
#define GPCP_INCLUDE_REGION_H_

#include <unordered_set>

#include "generator.h"
#include "intersect_point.h"
#include "tools.h"


#define NO_PLATE -1

enum Landscape {
  kOcean = 1,
  kCoast,
  kLand,
  kHill,
  kMountain,
};

class Region {
 public:
  explicit Region(Generator *generator);
  ~Region();

  // Getters
  Generator *GetGenerator () { return generator_; }
  int GetNumberOfIPoints () const { return number_of_ipoints_; }
  IntersectPoint **GetIPoints () { return i_points_; }
  int GetNumPlate() { return num_plate_belong_; }
  unsigned int GetId() { return id_; }
  int GetDownHillRegion() { return down_hill_region_; }
  int GetLandscapeType() { return static_cast<int>(landscape_type_); }
  glm::vec3 GetColor() { return color_; }
  std::unordered_set<int> *GetNeigborsRegions() { return &neighbors_regions_; }

  // Setter
  void SetNumPlateBelong(int num);

  // Allows us to hide methodes (SetAltitude, SetTypeRegion ...) to user
  // and to encapsulate these into region_manager
  friend class RegionManager;

 private:
  void SortIntersectPoints();
  void SetAltitude(float alt) { generator_->SetAltitude(alt); }
  void SetTypeRegion(float land_ratio);
  void SetDownHillRegion(int i) { down_hill_region_ = i; }
  void SetColor(glm::vec3 color) { color_ = color; }
  Generator *generator_;
  int number_of_ipoints_;
  IntersectPoint **i_points_;
  int num_plate_belong_;
  std::unordered_set<int> neighbors_regions_;

  int down_hill_region_;
  // Equivalent to generator Id
  unsigned int id_;
  Landscape landscape_type_;
  glm::vec3 color_;
};

#endif  // GPCP_INCLUDE_REGION_H_
