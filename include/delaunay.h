// "Copyright 2019 Billy Boys"
#ifndef GPCP_INCLUDE_DELAUNAY_H_
#define GPCP_INCLUDE_DELAUNAY_H_

#include <unordered_set>
#include <string>

#include "generator.h"
#include "parser.h"
#include "tools.h"
#include "triangle.h"


class Delaunay {
 public:
  explicit Delaunay(const std::string &filename);
  explicit Delaunay(const int number_of_generator);
  ~Delaunay();

  Generator **GetGeneratorArray() { return generator_array_; }
  int GetNumberGenerator() { return number_of_generators_; }
  std::unordered_set<Triangle*> *GetTriangleSet() {
    return &triangles_set_final_;
  }

  // Function which allows us to know if the triangulation is correct
  bool VerifyDelaunayTriangulation();
  bool VerifyGeneratorWithoutTriangle();

 private:
  // Generate "number" of random generators on the spĥere,
  // and fill generator_array with it.
  // Called when the user didn't give a file of generators during execution.
  void GenerateRandUniform(int number);
  void ConstructDelaunayTriangulation();
  int number_of_generators_;
  Generator **generator_array_;
  std::unordered_set<Triangle*> triangles_set_final_;
};

#endif  // GPCP_INCLUDE_DELAUNAY_H_
