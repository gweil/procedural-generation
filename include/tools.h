// "Copyright 2019 Billy Boys"
#ifndef GPCP_INCLUDE_TOOLS_H_
#define GPCP_INCLUDE_TOOLS_H_

#include <string>

#include "glm/vec2.hpp"
#include "glm/vec3.hpp"


class Tools {
 public:
  static bool IsNumber(char *c);
  static double GetNorm(glm::dvec3 vector);
  static double GetNorm(glm::dvec2 vector);
  static glm::dvec3 Vector3DNormalize(glm::dvec3 vector, double radius);
  static glm::dvec2 Vector2DNormalize(glm::dvec2 vector, double radius);

  // Vector computation tool kit
  static double Dot(glm::dvec3 vec1, glm::dvec3 dvec2);
  static double Dot(glm::dvec2 vec1, glm::dvec2 dvec2);
  static glm::dvec3 Cross(glm::dvec3 vec1, glm::dvec3 dvec2);

  // OpenGL subfunctions
  static void CreateLine(glm::dvec3 p1, glm::dvec3 p2);
  static void PrintArc(glm::dvec3 point1, glm::dvec3 point2, double radius,
                       glm::dvec3 color, double divi);
  static std::string NameGen();

 private:
  Tools() = default;
};

#endif  // GPCP_INCLUDE_TOOLS_H_
