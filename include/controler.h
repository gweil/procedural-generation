// "Copyright 2019 Billy Boys"
#ifndef GPCP_INCLUDE_CONTROLER_H_
#define GPCP_INCLUDE_CONTROLER_H_

#include <unordered_set>
#include <utility>
#include <vector>

#include "glm/vec3.hpp"
#include "voronoi.h"


class Controler {
 public:
  Controler(int nb_generators, int nb_plates);
  Controler(char *filename, int nb_plates);
  ~Controler();

  void EventOnPress(unsigned char event[3]);
  bool VerifyVoronoiDiagram();
  void ExportDiagram();
  glm::vec3 CoordIPoint(uint index_r, uint index_i);
  bool RegionIndexIsValid(int index_region);
  bool RegionIsBorder(int index_region);

  std::array<glm::vec3, 3> GetTrianglePoints(int counter);
  glm::vec3 GetRegionColor(int index_region);
  int GetNumberGenerator();
  int GetNbTriangles();
  int GetTemperature();
  int GetMoisture();
  glm::vec3 GetPlateColorOfRegion(int index_region);
  std::vector<std::pair<IntersectPoint*, IntersectPoint*>> GetBorderPoints();
  int GetRegionDownHillIndex(int index_region);
  int GetRegionLandscape(int index_region);
  float GetRegionAltitude(int index_region);
  int GetPerlinOctaves();
  float GetSeaLevel();

  Region *GetRegion(int index_region);
  std::unordered_set <int> GetSelectedRivers();
  glm::vec3 **GetRiversSegmentations();
  int GetRiversSegmentDivisions();

 private:
  Voronoi *voronoi_;
  std::unordered_set<Triangle*>::iterator triangles_it_;
  int number_of_triangles_;
};

#endif  // GPCP_INCLUDE_CONTROLER_H_
