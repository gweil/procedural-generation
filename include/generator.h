// "Copyright 2019 Billy Boys"
#ifndef GPCP_INCLUDE_GENERATOR_H_
#define GPCP_INCLUDE_GENERATOR_H_

#include <unordered_set>

#include "point.h"
#include "triangle.h"


#define RADIUS 1

// The generator will be used in Voronoi and in Delaunay triangulation.
// In delaunay triangulation, it will be the vertices of the triangle.
class Generator : public Point {
 public:
  Generator(double, double, double, unsigned int);
  Generator(glm::vec3, double, unsigned int);
  ~Generator();
  glm::vec3 GetCartesianCoord() const { return glm::dvec3(x(), y(), z()); }
  std::unordered_set<Triangle*, std::TriangleHasher, std::TriangleComp>
  GetBelongingTriangles() {
    return belonging_triangles_;
  }

  unsigned int GetId() const { return id_; }

  int GetNumberBelongingTriangles() {
    return belonging_triangles_.size();
  }

  bool operator ==(const Generator & obj) const {
    return (GetPhi() == obj.GetPhi()
        && GetTheta() == obj.GetTheta()
        && GetAltitude() == obj.GetAltitude());
  }

  // Allows us to hide methodes (AddTriangle and RemoveTriangle) to user.
  friend class Triangle;

 private:
  void AddTriangle(Triangle *triangle);
  void RemoveTriangle(Triangle *triangle);
  void SetCartesianCoord();
  void SetPolarCoord();
  std::unordered_set<Triangle*,
                     std::TriangleHasher,
                     std::TriangleComp> belonging_triangles_;
  unsigned int id_;
};



#endif  // GPCP_INCLUDE_GENERATOR_H_
