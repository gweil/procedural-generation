// "Copyright 2019 Billy Boys"
#ifndef GPCP_INCLUDE_POINT_H_
#define GPCP_INCLUDE_POINT_H_

#include <iostream>


class Point {
 public:
  double GetPhi() const { return phi_; }
  double GetTheta() const { return theta_; }
  double GetAltitude() const { return altitude_; }
  double x() const { return x_; }
  double y() const { return y_; }
  double z() const { return z_; }
  void SetAltitude(double alt) { altitude_ = alt; }

  bool operator ==(const Point & obj) const {
    return (x_ == obj.x()
        && y_ == obj.y()
        && z_ == obj.z());
  }

 protected:
  double phi_;
  double theta_;
  double altitude_;
  double x_;
  double y_;
  double z_;
};

#endif  // GPCP_INCLUDE_POINT_H_
