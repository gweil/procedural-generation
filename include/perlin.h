// "Copyright 2019 Billy Boys"
#ifndef GPCP_INCLUDE_PERLIN_H_
#define GPCP_INCLUDE_PERLIN_H_

#include <cmath>

#include "glm/vec2.hpp"


#define DEFAULT_NB_OCTAVES 3

class Perlin {
 public:
  Perlin();
  ~Perlin();

  float ComputePerlinNoise(float x, float y, float z);
  void DecrementOctaves();
  void IncrementOctaves();
  void SetNumberOctaves(int nb_octaves);
  int GetOctaves();

 private:
  void ComputeNoiseGrid();
  void DeleteGrid();
  float DotGridGradient(int iu, int iv, float u, float v, int octave);
  float Lerp(float a0, float a1, float w);

  glm::vec2 ***noise_grid_;
  int nb_octaves_;
};

#endif  // GPCP_INCLUDE_PERLIN_H_
