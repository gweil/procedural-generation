// "Copyright 2019 Billy Boys"
#ifndef GPCP_INCLUDE_PLATE_H_
#define GPCP_INCLUDE_PLATE_H_

#include <unordered_set>

#include "region.h"


class Plate{
 public:
  Plate() = default;
  ~Plate() = default;

  void AddRegion(int id_region);
  void SetColor(glm::vec3 colors) { colors_ = colors;}

  glm::vec3 GetColor() { return colors_; }
  std::unordered_set<int> GetRegions() { return index_regions_; }

 private:
  std::unordered_set<int> index_regions_;
  glm::vec3 colors_;
};

#endif  // GPCP_INCLUDE_PLATE_H_
