// "Copyright 2019 Billy Boys"
#ifndef GPCP_INCLUDE_TECTONIC_PLATES_H_
#define GPCP_INCLUDE_TECTONIC_PLATES_H_

#include <unordered_set>
#include <utility>
#include <vector>

#include "plate.h"
#include "region_manager.h"


class TectonicPlates {
 public:
  TectonicPlates(int numberOfPlate, int number_of_generators,
                 RegionManager *region_manager);
  ~TectonicPlates();

  bool RegionIsBorder(int index_region);

  // Getters
  int GetNumberOfPlates() { return number_of_plates_;}
  glm::vec3 GetPlateColor(int index_plate);
  glm::vec3 GetPlateColorOfRegion(int index_region);
  std::unordered_set<int> GetRegionsOfPlate(int index_plate);
  std::vector<std::pair<IntersectPoint*, IntersectPoint*>> GetBorderPoints();

 private:
  std::unordered_set<int> SelectStartingRegions();
  void AffectToEachRegionAPlate();
  void SetBorderRegions();
  void SetBorderPoints();
  void DefineColorPlates();
  int number_of_plates_;
  int number_of_regions_;
  Plate **plates_;
  RegionManager *region_manager_;
  std::unordered_set<int> border_regions_;
  std::vector<std::pair<IntersectPoint*, IntersectPoint*>> border_points_;
};

#endif  // GPCP_INCLUDE_TECTONIC_PLATES_H_
