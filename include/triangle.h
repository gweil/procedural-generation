// "Copyright 2019 Billy Boys"
#ifndef GPCP_INCLUDE_TRIANGLE_H_
#define GPCP_INCLUDE_TRIANGLE_H_

#include <unordered_set>

#include "glm/vec3.hpp"
#include "tools.h"


class Generator;
// Base of delaunay triangulation.
// It will use the generators as vertices
class Triangle {
 public:
  Triangle(Generator *vertex0, Generator *vertex1, Generator *vertex2);
  ~Triangle();

  // Tests
  bool InCircumscribedSphere(const Generator* generator) const;
  bool IsGeneratorInTriangle(const Generator* generator) const;
  bool SameVertices(const Triangle &other_triangle) const;
  // Getters
  double GetRadius() const { return radius_; }
  glm::vec3 GetCircumcenter() const { return circumcenter_; }
  Generator **GetVertices() { return vertices_; }
  Generator *GetGenerator(int numero_generator) const;
  uint64_t GetId() const { return id_; }

  // Comparators
  bool operator ==(const Triangle & obj) const {
    return (GetId() == obj.GetId() || SameVertices(obj));
  }

 private:
  void SetCircumcenterRadius();
  void SetVertices(Generator *vertex0, Generator *vertex1, Generator *vertex2);
  void SetId() { id_ = (uint64_t)this; }

  double radius_;
  glm::dvec3 circumcenter_;
  Generator **vertices_;
  uint64_t id_;
};


namespace std {
// Use during the hash
template<> struct hash<Triangle*> {
  size_t operator()(const Triangle * obj) const {
    size_t res = hash<uint64_t>()(
      (uint64_t)(obj->GetGenerator(0)) +
      (uint64_t)(obj->GetGenerator(1)) +
      (uint64_t)(obj->GetGenerator(2)));
    return res;
  }
};


struct TriangleHasher {
  size_t operator()(const Triangle * obj) const {
    size_t res = hash<uint64_t>()(
      (uint64_t)(obj->GetGenerator(0)) +
      (uint64_t)(obj->GetGenerator(1)) +
      (uint64_t)(obj->GetGenerator(2)));
    return res;
  }
};


struct TriangleComp {
  bool operator()(const Triangle *obj1, const Triangle *obj2) const {
    return *obj1 == *obj2;
  }
};
}  // namespace std

#endif  // GPCP_INCLUDE_TRIANGLE_H_
