// "Copyright 2019 Billy Boys"
#ifndef GPCP_INCLUDE_RIVERS_H_
#define GPCP_INCLUDE_RIVERS_H_

#include <unordered_set>
#include <vector>

#include "glm/vec3.hpp"
#include "region.h"


class Rivers {
 public:
  Rivers(std::unordered_set<int> rivers, Region **regions,
         int number_of_regions);
  ~Rivers();

  void CreateSegmentation();

  std::unordered_set <int> GetSelectedRivers() { return selected_rivers_; }
  glm::vec3 **GetSegmentations() { return segmentations_; }
  int GetNumberRivers() { return number_of_rivers_; }
  int GetDiv() { return div_; }

 private:
  std::unordered_set<int> selected_rivers_;
  int number_of_rivers_;
  Region **regions_;
  glm::vec3 **segmentations_;
  int div_;
};

#endif  // GPCP_INCLUDE_RIVERS_H_
