// "Copyright 2019 Billy Boys"
#ifndef GPCP_INCLUDE_VORONOI_H_
#define GPCP_INCLUDE_VORONOI_H_

#include <string>
#include <unordered_set>

#include "delaunay.h"
#include "region.h"
#include "region_manager.h"
#include "tectonic_plates.h"


class Voronoi {
 public:
  Voronoi(const std::string &filename, const int number_of_plates);
  Voronoi(const int number_of_generator, const int number_of_plates);
  ~Voronoi();

  Region **GenerateRegion();
  bool VerifyVoronoiDiagram();

  Delaunay *GetDelaunay() { return delaunay_; }
  int GetNumberGenerator() { return number_of_generators_; }
  RegionManager *GetRegionManager() { return region_manager_; }
  TectonicPlates *GetTectonicPlates() { return tectonic_plates_; }

 private:
  Delaunay *delaunay_;
  int number_of_generators_;
  Region **regions_;
  TectonicPlates *tectonic_plates_;
  RegionManager *region_manager_;
};

#endif  // GPCP_INCLUDE_VORONOI_H_
