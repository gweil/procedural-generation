// "Copyright 2019 Billy Boys"
#ifndef GPCP_INCLUDE_REGION_MANAGER_H_
#define GPCP_INCLUDE_REGION_MANAGER_H_

#include <algorithm>
#include <unordered_set>

#include "perlin.h"
#include "region.h"
#include "rivers.h"


enum {
  kFrosty,
  kCold,
  kTemperate,
  kHot,
  kHottest,
};

enum {
  kSuperarid,
  kArid,
  kNormal,
  kHumid,
  kSuperhumid,
};

#define GAP_SEA_LEVEL 0.05f
#define MAX_SEA_LEVEL 0.8f
#define MIN_SEA_LEVEL 0.3f


class RegionManager{
 public:
  RegionManager(Region **regions, int number_of_regions);
  ~RegionManager();

  int GetTemperature() {return temperature_;}
  int GetMoisture() {return moisture_;}
  float GetSeaLevel() {return sea_level_;}
  int GetPerlinOctaves() {return perlin_->GetOctaves();}

  void RefreshColors();

  // Buttons UI
  void IncWaterLevel();
  void DecWaterLevel();
  void IncTemperature();
  void DecTemperature();
  void IncMoisture();
  void DecMoisture();
  void IncrementOctaves();
  void DecrementOctaves();

  // Setters
  void SetRegionAltitudes();
  std::unordered_set<int> SetDownHillRegion();
  void SetNumberOctaves(int nb_octaves);

  // Getters
  Region *GetRegion(int index_region);
  glm::vec3 GetRegionColor(int index_region);
  Rivers *GetRivers() { return rivers_; }
  int GetNumberRegion();

 private:
  int temperature_;
  int moisture_;
  float sea_level_;
  Region **regions_;
  int number_of_regions_;
  glm::vec3 color_palette_[5][5][4];
  Rivers *rivers_;
  Perlin *perlin_;
};

#endif  // GPCP_INCLUDE_REGION_MANAGER_H_
