// "Copyright 2019 Billy Boys"
#ifndef GPCP_INCLUDE_PARSER_H_
#define GPCP_INCLUDE_PARSER_H_

#include <string>

#include "generator.h"


class Parser {
 public:
  static int CountGenerator(const std::string &filename);

  // Write the generators from generator_array_ to filename
  static void ExportGenerators(const std::string &filename,
    Generator **generators_array,
    int number_of_generators);

  // Read the file text of generators
  // and fill generator_array_ with it.
  static void LoadGeneratorFromTXT(const std::string &filename,
                                   Generator **generators_array);

 private:
  Parser(){}
};

#endif  // GPCP_INCLUDE_PARSER_H_
