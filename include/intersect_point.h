// "Copyright 2019 Billy Boys"
#ifndef GPCP_INCLUDE_INTERSECT_POINT_H_
#define GPCP_INCLUDE_INTERSECT_POINT_H_

#include "glm/vec3.hpp"
#include "point.h"


class IntersectPoint : public Point {
 public:
  IntersectPoint();
  explicit IntersectPoint(glm::vec3 coordinates){
    x_ = coordinates[0];
    y_ = coordinates[1];
    z_ = coordinates[2];
    altitude_ = 0;
  }
};

#endif  // GPCP_INCLUDE_INTERSECT_POINT_H_
