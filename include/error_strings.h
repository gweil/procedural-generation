// "Copyright 2019 Billy Boys"
#ifndef GPCP_INCLUDE_ERROR_STRINGS_H_
#define GPCP_INCLUDE_ERROR_STRINGS_H_

#include <string>


#define SCS static const std::string

SCS parser_CountGenerator = "Parser CountGenerator: ";

SCS parser_LoadGeneratorFromTXT = "Parser LoadGeneratorFromTXT: ";


SCS parser_datainput_ERROR = std::string("Parser LoadGeneratorFromTXT: Wrong ")
                         + "data in file. Make sure you respect the format "
                         + "t=... p=... alt=...";


SCS techtonicplates_datainput_ERROR = std::string("Invalid parameters in")
                                      + "TechtonicPlates::DefineColorPlates: "
                                      + "Number of plate must be < 18 due to "
                                      + "restricted number of color";

SCS number_of_plates_ERROR =
    "number_of_plate must be < number_of_generators/10";

SCS number_of_generators_ERROR = "number_of_generators must be > 0";

SCS voronoi_datainput_ERROR = "Invalid parameters in Voronoi: ";

SCS delaunay_triangulation_ERROR = std::string("Wrong Delaunay Triangulation :")
                                   + "Generators without triangle";

SCS delaunay_FindOtherCommonTriangle_ERROR = std::string("Invalid parameters ")
                                             + "in FindOtherCommonTriangle: "
                                             + "Generator not in triangle";
SCS delaunay_GenerateAleatUniform_ERROR = std::string("Invalid parameters in ")
                                          + "GenerateAleatUniform: Incorrect "
                                          + "of random seeds. It must be > 4";

SCS region_SetNumPlateBelong_ERROR = std::string("Invalid parameters in ")
                                     + "Region::SetNumPlateBelong : Number of "
                                     + "plate must be >= 0";
SCS file404_ERROR = "File doesn't exist";

SCS openfile_ERROR = "Can't open file: ";

#endif  // GPCP_INCLUDE_ERROR_STRINGS_H_
