// "Copyright 2019 Billy Boys"
#include "tools.h"

#include <cmath>
#include <cstring>
#include <ctime>
#include <GL/glew.h>


// Function used in gpcp.cpp in order to determine if the user gave
// a number of generator to generate randomly during the execution
bool Tools::IsNumber(char *c) {
  for (unsigned int i = 0; i < strlen(c); ++i) {
    if (!isdigit(c[i])) {
      return false;
    }
  }
  return true;
}


double Tools::GetNorm(glm::dvec3 vector) {
  return std::sqrt(vector[0] * vector[0]
                   + vector[1] * vector[1]
                   + vector[2] * vector[2]);
}


double Tools::GetNorm(glm::dvec2 vector) {
  return std::sqrt(vector[0] * vector[0] + vector[1] * vector[1]);
}


glm::dvec3 Tools::Vector3DNormalize(glm::dvec3 vector, double radius) {
  double norm = GetNorm(vector);
  if (norm != 0) {
    vector[0] /= norm;
    vector[0] *= radius;

    vector[1] /= norm;
    vector[1] *= radius;

    vector[2] /= norm;
    vector[2] *= radius;
  }
  return vector;
}


glm::dvec2 Tools::Vector2DNormalize(glm::dvec2 vector, double radius) {
  double norm = GetNorm(vector);
  if (norm != 0) {
    vector[0] /= norm;
    vector[0] *= radius;

    vector[1] /= norm;
    vector[1] *= radius;
  }
  return vector;
}


double Tools::Dot(glm::dvec3 vec1, glm::dvec3 dvec2) {
  return vec1.x * dvec2.x + vec1.y * dvec2.y + vec1.z * dvec2.z;
}


double Tools::Dot(glm::dvec2 vec1, glm::dvec2 dvec2) {
  return vec1.x * dvec2.x + vec1.y * dvec2.y;
}


glm::dvec3 Tools::Cross(glm::dvec3 vec1, glm::dvec3 dvec2) {
  glm::dvec3 output(vec1[1] * dvec2[2] - vec1[2] * dvec2[1],
                   vec1[2] * dvec2[0] - vec1[0] * dvec2[2],
                   vec1[0] * dvec2[1] - vec1[1] * dvec2[0]);
  return output;
}


void Tools::CreateLine(glm::dvec3 p1, glm::dvec3 p2) {
  glLineWidth(1.0f);
  glBegin(GL_LINES);
  glVertex3f(p1[0], p1[1], p1[2]);
  glVertex3f(p2[0], p2[1], p2[2]);
  glEnd();
}


void Tools::PrintArc(glm::dvec3 point1, glm::dvec3 point2, double radius,
                     glm::dvec3 color, double divi) {
  glLineWidth(1.5f);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glColor3f(color.x, color.y, color.z);
  glBegin(GL_LINES);
  glm::dvec3 p1(point1[0], point1[1], point1[2]);
  glm::dvec3 p2(point2[0], point2[1], point2[2]);
  glm::dvec3 tmp(0.0, 0.0, 0.0);
  glm::dvec3 vect(p2[0] - p1[0], p2[1] - p1[1], p2[2] - p1[2]);
  glm::dvec3 subvect(vect[0] / divi, vect[1] / divi, vect[2] / divi);

  for (int i = 0; i < divi; i++) {
    tmp[0] = p1[0];
    tmp[1] = p1[1];
    tmp[2] = p1[2];
    tmp = Vector3DNormalize(tmp, radius);
    glVertex3f(tmp[0], tmp[1], tmp[2]);
    p1[0] += subvect[0];
    p1[1] += subvect[1];
    p1[2] += subvect[2];

    tmp[0] = p1[0];
    tmp[1] = p1[1];
    tmp[2] = p1[2];
    tmp = Vector3DNormalize(tmp, radius);
    glVertex3f(tmp[0], tmp[1], tmp[2]);
  }
  glEnd();
}


std::string Tools::NameGen() {
  time_t timer;
  uint seed = static_cast<uint>(time(&timer));
  static std::string nameprefix[7] = {"", "bel", "nar", "xan", "bell", "natr",
                                      "ev"};
  static std::string namestems[20] = {"adur", "aes", "anim", "apoll", "imac",
                                      "educ", "equis", "extr", "guius", "hann",
                                      "equi", "amora", "hum", "iace", "ille",
                                      "inept", "iuv", "obe", "ocul", "orbis"};
  static std::string namesuffix[16] = {"", "us", "ix", "ox", "ith", "ath", "um",
                                       "ator", "or", "axia", "imus", "ais",
                                       "itur", "orex", "o", "y"};
  std::string name = "";
  name = nameprefix[(rand_r(&seed) % 7)];
  name += namestems[(rand_r(&seed) % 20)];
  name += namesuffix[(rand_r(&seed) % 16)];
  name[0] = toupper(name[0]);
  return name;
}
