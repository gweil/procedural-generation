// "Copyright 2019 Billy Boys"
#include "rivers.h"

#include <ctime>
#include <random>


Rivers::Rivers(std::unordered_set<int> rivers, Region **regions,
               int number_of_regions) {
  auto rivers_iterator = rivers.begin();
  while (rivers_iterator != rivers.end()) {
    int region_num_1 = *rivers_iterator;
    std::unordered_set<int> tmp;
    int erase = 0;

    while (1) {
      tmp.insert(region_num_1);
      if (regions[region_num_1]->GetDownHillRegion() == -1) {
        break;
      }
      int region_num_2 = regions[region_num_1]->GetDownHillRegion();
      if (tmp.find(region_num_2) != tmp.end()) {
        erase = 1;
        break;
      } else {
        region_num_1 = region_num_2;
      }
    }
    if (erase == 1)
      rivers_iterator = rivers.erase(rivers_iterator);
    else
      rivers_iterator++;
  }
  selected_rivers_ = rivers;
  regions_ = regions;

  div_ = std::max(10 - (number_of_regions/1004), 3);
  CreateSegmentation();
}


Rivers::~Rivers() {
  regions_ = nullptr;
  for (unsigned int count = 0; count < selected_rivers_.size(); ++count) {
    delete[] segmentations_[count];
  }
  delete[] segmentations_;
}


void Rivers::CreateSegmentation() {
  time_t timer;
  uint seed = static_cast<uint>(time(&timer));
  std::mt19937 generator(seed);
  std::uniform_real_distribution<double> uniform01(0.0, 1.0);

  segmentations_ = new glm::vec3*[selected_rivers_.size()];
  for (unsigned int i = 0; i < selected_rivers_.size(); i++)
    segmentations_[i] = new glm::vec3[div_];

  int counter = 0;
  for (int generator_number : selected_rivers_) {
    int region_num_1 = generator_number;
    int region_num_2 = regions_[region_num_1]->GetDownHillRegion();
    glm::dvec3 p1 = regions_[region_num_1]->GetGenerator()->GetCartesianCoord();
    glm::dvec3 p2 = regions_[region_num_2]->GetGenerator()->GetCartesianCoord();
    double norm = Tools::GetNorm(p2 - p1);
    glm::dvec3 p12_cross = Tools::Vector3DNormalize(Tools::Cross(p1, p2 - p1),
                                                norm/static_cast<double>(div_));
    segmentations_[counter][0] = p1;
    for (int i = 1; i < div_ - 1; i++) {
      glm::dvec3 point = (static_cast<double>(i) * (p2 - p1));
      point = point / static_cast<double>(div_) + p1;
      point += (static_cast<double>(uniform01(generator)))*p12_cross;
      segmentations_[counter][i] = point;
    }
    segmentations_[counter][div_ - 1] = p2;
    counter++;
  }
}
