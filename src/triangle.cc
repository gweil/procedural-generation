// "Copyright 2019 Billy Boys"
#include "triangle.h"

#include <cmath>

#include "generator.h"


Triangle::Triangle(Generator *vertex0, Generator *vertex1, Generator *vertex2) {
  if (vertex0 == vertex1 || vertex0 == vertex2 || vertex1 == vertex2) {
    throw std::invalid_argument(
      "Invalid parameters of Triangle : identicals vertices");
  }
  SetVertices(vertex0, vertex1, vertex2);
  SetCircumcenterRadius();
  SetId();

  vertex0->AddTriangle(this);
  vertex1->AddTriangle(this);
  vertex2->AddTriangle(this);
}


// Beware when you destruct triangle
// it must be before its generators
Triangle::~Triangle() {
  for (int i = 0; i < 3; ++i) {
    vertices_[i]->RemoveTriangle(this);
  }
  delete[] vertices_;
  vertices_ = nullptr;
}


Generator *Triangle::GetGenerator(int numero_generator) const {
  return vertices_[numero_generator];
}


bool Triangle::InCircumscribedSphere(const Generator* generator) const {
  glm::dvec3 coord_generator = generator->GetCartesianCoord();
  // Comparing generator to circumcenter distance with the circumsphere radius
  double distance_generator_circumcenter = sqrt((coord_generator[0]
                            - circumcenter_[0]) * (coord_generator[0]
                            - circumcenter_[0]) + (coord_generator[1]
                            - circumcenter_[1]) * (coord_generator[1]
                            - circumcenter_[1]) + (coord_generator[2]
                            - circumcenter_[2]) * (coord_generator[2]
                            - circumcenter_[2]));

  return radius_ - distance_generator_circumcenter > 1.0e-8;
}


bool Triangle::IsGeneratorInTriangle(const Generator * generator) const {
  for (int i = 0; i < 3; i++) {
    if (GetGenerator(i) == generator) {
      return true;
    }
  }
  return false;
}


void Triangle::SetCircumcenterRadius() {
  glm::dvec3 coord_a = vertices_[0]->GetCartesianCoord();
  glm::dvec3 coord_b = vertices_[1]->GetCartesianCoord();
  glm::dvec3 coord_c = vertices_[2]->GetCartesianCoord();

  glm::dvec3 vect_ab = coord_b-coord_a;
  glm::dvec3 vect_ac = coord_c-coord_a;

  double dot_aa, dot_bb, dot_ab;
  dot_aa = Tools::Dot(vect_ab, vect_ab);
  dot_bb = Tools::Dot(vect_ac, vect_ac);
  dot_ab = Tools::Dot(vect_ab, vect_ac);

  double base = 0.5 / (dot_aa * dot_bb-dot_ab * dot_ab);
  double x1 = base * dot_bb * (dot_aa-dot_ab);
  double x2 = base * dot_aa * (dot_bb-dot_ab);
  circumcenter_ = coord_a + vect_ab * x1 + vect_ac * x2;
  glm::dvec3 diff = circumcenter_ - coord_b;
  radius_ = sqrt(Tools::Dot(diff, diff));
}


void Triangle::SetVertices(Generator *vertex0, Generator *vertex1,
                           Generator *vertex2) {
  vertices_ = new Generator*[3];
  vertices_[0] = vertex0;
  vertices_[1] = vertex1;
  vertices_[2] = vertex2;
}


bool Triangle::SameVertices(const Triangle &other_triangle) const {
  for (int num_generator = 0; num_generator < 3; num_generator++) {
    bool same_generator = false;
    for (int num_gen_other = 0; num_gen_other < 3; num_gen_other++) {
      Generator gen = *GetGenerator(num_generator);
      Generator other_gen = *other_triangle.GetGenerator(num_gen_other);
      if (gen == other_gen) {
        same_generator = true;
      }
    }
    if (!same_generator) {
      return false;
    }
  }
  return true;
}
