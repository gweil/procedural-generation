// "Copyright 2019 Billy Boys"
#include "region_manager.h"


RegionManager::RegionManager(Region **regions, int number_of_regions) {
  temperature_ = kTemperate;
  moisture_ = kNormal;
  sea_level_ = 0.5f;
  regions_ = regions;
  number_of_regions_ = number_of_regions;
  rivers_ = nullptr;
  perlin_ = new Perlin();

  glm::vec3 color1(0.14, 0.37, 0.15);
  glm::vec3 color2(0.17, 0.47, 0.19);
  glm::vec3 color3(0.20, 0.51, 0.22);
  glm::vec3 color4(0.22, 0.56, 0.24);
  glm::vec3 color5(0.26, 0.63, 0.28);
  glm::vec3 color6(0.56, 0.76, 0.00);
  glm::vec3 color7(0.82, 0.95, 0.92);
  glm::vec3 color8(0.64, 0.89, 0.84);
  glm::vec3 color9(0.28, 0.79, 0.69);
  glm::vec3 color10(0.07, 0.48, 0.40);
  glm::vec3 color11(0.04, 0.33, 0.27);
  glm::vec3 color12(0.16, 0.45, 0.65);
  glm::vec3 color13(0.52, 0.76, 0.91);
  glm::vec3 color14(0.68, 0.84, 0.95);
  glm::vec3 color15(0.84, 0.92, 0.97);
  glm::vec3 color16(0.96, 0.82, 0.25);
  glm::vec3 color17(0.96, 0.69, 0.25);
  glm::vec3 color18(0.95, 0.61, 0.07);
  glm::vec3 color19(0.90, 0.49, 0.13);
  glm::vec3 color20(0.30, 0.69, 0.31);
  glm::vec3 color21(0.92, 0.96, 0.98);
  glm::vec3 color22(0.00, 0.90, 0.00);
  glm::vec3 color23(0.55, 0.55, 0.55);
  glm::vec3 color24(0.65, 0.65, 0.65);
  glm::vec3 color25(0.75, 0.75, 0.75);
  glm::vec3 color26(0.85, 0.85, 0.85);
  glm::vec3 color27(1.00, 1.00, 1.00);

  glm::vec3 color_palette[5][5][4] = {
    { {color15, color14, color13, color27},
      {color16, color15, color14, color26},
      {color16, color16, color20, color25},
      {color16, color16, color17, color24},
      {color16, color17, color18, color23} },
    { {color15, color21, color14, color27},
      {color16, color21, color7, color26},
      {color16, color20, color20, color25},
      {color16, color16, color20, color24},
      {color16, color16, color17, color23} },
    { {color15, color7, color14, color27},
      {color16, color7, color8, color26},
      {color16, color20, color5, color25},
      {color16, color20, color4, color24},
      {color16, color16, color4, color23} },
    { {color15, color7, color9, color27},
      {color16, color8, color9, color26},
      {color16, color5, color4, color25},
      {color16, color4, color3, color24},
      {color16, color3, color2, color23} },
    { {color15, color9, color10, color27},
      {color16, color9, color9, color26},
      {color16, color4, color3, color25},
      {color16, color3, color2, color24},
      {color16, color2, color1, color23} }
  };

  for (int i = 0; i < 5; i++)
    for (int y = 0; y < 5; y++)
      for (int z = 0; z < 4; z++)
        color_palette_[i][y][z] = color_palette[i][y][z];

  SetRegionAltitudes();
}


RegionManager::~RegionManager() {
  regions_ = nullptr;
  delete rivers_;
  delete perlin_;
}


void RegionManager::IncWaterLevel() {
  sea_level_ = std::fmin(sea_level_ + GAP_SEA_LEVEL, MAX_SEA_LEVEL);
  for (int nb_region = 0; nb_region < number_of_regions_; ++nb_region) {
    regions_[nb_region]->SetTypeRegion(sea_level_);
  }
  RefreshColors();
  delete rivers_;
  std::unordered_set <int> all_rivers = SetDownHillRegion();
  rivers_ = new Rivers (all_rivers, regions_, number_of_regions_);
}


void RegionManager::DecWaterLevel() {
  sea_level_ = std::fmax(sea_level_ - GAP_SEA_LEVEL, MIN_SEA_LEVEL);
  for (int nb_region = 0; nb_region < number_of_regions_; ++nb_region) {
    regions_[nb_region]->SetTypeRegion(sea_level_);
  }
  RefreshColors();
  delete rivers_;
  std::unordered_set <int> all_rivers = SetDownHillRegion();
  rivers_ = new Rivers (all_rivers, regions_, number_of_regions_);
}


void RegionManager::SetRegionAltitudes() {
  for (int nb_region = 0; nb_region < number_of_regions_; ++nb_region) {
    Generator *gen = regions_[nb_region]->GetGenerator();
    glm::vec3 v = gen->GetCartesianCoord();
    gen->SetAltitude(perlin_->ComputePerlinNoise(v.x, v.y, v.z));
    regions_[nb_region]->SetTypeRegion(sea_level_);
  }
  RefreshColors();
  delete rivers_;
  std::unordered_set <int> all_rivers = SetDownHillRegion();
  rivers_ = new Rivers (all_rivers, regions_, number_of_regions_);
}


std::unordered_set<int> RegionManager::SetDownHillRegion() {
  std::unordered_set<int> rivers;
  for (int nb_region = 0; nb_region < number_of_regions_; ++nb_region) {
    Region * region = regions_[nb_region];
    if (region->GetLandscapeType() == 1) {
      region->SetDownHillRegion(-1);
      continue;
    }
    float min = 10;
    int min_index = -1;
    for (auto index : *(region->GetNeigborsRegions())) {
      float altitude = regions_[index]->GetGenerator()->GetAltitude();
      if (min > altitude) {
        min = altitude;
        min_index = index;
      }
    }
    rivers.insert(nb_region);
    region->SetDownHillRegion(min_index);
  }
  return rivers;
}


void RegionManager::IncTemperature() {
  temperature_ = std::min(static_cast<int>(kHottest), temperature_ + 1);
  RefreshColors();
}


void RegionManager::DecTemperature() {
  temperature_ = std::max(static_cast<int>(kFrosty), temperature_ - 1);
  RefreshColors();
}


void RegionManager::IncMoisture() {
  moisture_ = std::min(static_cast<int>(kSuperhumid), moisture_ + 1);
  RefreshColors();
}


void RegionManager::DecMoisture() {
  moisture_ = std::max(static_cast<int>(kSuperarid), moisture_ - 1);
  RefreshColors();
}


void RegionManager::RefreshColors() {
  for (int nb_region = 0; nb_region < number_of_regions_; ++nb_region) {
    int region_landscape_type = regions_[nb_region]->GetLandscapeType();
    double region_alt = regions_[nb_region]->GetGenerator()->GetAltitude();
    glm::vec3 color = glm::vec3(1);
    switch (region_landscape_type) {
      case kOcean: {
        color = glm::vec3(0.0 , 0.0, 1 - fabs(region_alt));
        break;
      }
      case kCoast: {
        color = color_palette_[moisture_][temperature_][0];
        color.x -= fabs(region_alt)*1.5;
        color.y -= fabs(region_alt)*1.5;
        color.z -= fabs(region_alt)*1.5;
        break;
      }
      case kLand: {
        color = color_palette_[moisture_][temperature_][1];
        color.x -= fabs(region_alt);
        color.y -= fabs(region_alt);
        color.z -= fabs(region_alt);
        break;
      }
      case kHill: {
        color = color_palette_[moisture_][temperature_][2];
        color.x -= fabs(region_alt)*0.75;
        color.y -= fabs(region_alt)*0.75;
        color.z -= fabs(region_alt)*0.75;
        break;
      }
      case kMountain: {
        color = color_palette_[moisture_][temperature_][3];
        color.x -= fabs(region_alt)*0.5;
        color.y -= fabs(region_alt)*0.5;
        color.z -= fabs(region_alt)*0.5;
        break;
      }
      default: {
        color = glm::vec3(1);
        break;
      }
    }
    regions_[nb_region]->SetColor(color);
  }
}


glm::vec3 RegionManager::GetRegionColor(int index_region) {
  return regions_[index_region]->GetColor();
}


int RegionManager::GetNumberRegion() {
  return number_of_regions_;
}


void RegionManager::SetNumberOctaves(int nb_octaves) {
  perlin_->SetNumberOctaves(nb_octaves);
  SetRegionAltitudes();
}


void RegionManager::IncrementOctaves() {
  perlin_->IncrementOctaves();
  SetRegionAltitudes();
}


void RegionManager::DecrementOctaves() {
  perlin_->DecrementOctaves();
  SetRegionAltitudes();
}


Region *RegionManager::GetRegion(int index_region) {
  if (index_region < number_of_regions_)
    return regions_[index_region];
  return nullptr;
}
