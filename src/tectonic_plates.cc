// "Copyright 2019 Billy Boys"
#include "tectonic_plates.h"

#include <ctime>


TectonicPlates::TectonicPlates(int number_of_plate, int number_of_generators,
                               RegionManager *regionmanager) {
  number_of_plates_ = number_of_plate;
  number_of_regions_ = number_of_generators;
  region_manager_ = regionmanager;

  plates_ = new Plate * [number_of_plates_];
  for (int index = 0; index < number_of_plates_; index++) {
    plates_[index] = new Plate();
  }
  AffectToEachRegionAPlate();
  SetBorderRegions();
  SetBorderPoints();
  DefineColorPlates();
}


TectonicPlates::~TectonicPlates() {
  region_manager_ = nullptr;
  if (plates_ != nullptr) {
    for (int i = 0; i < number_of_plates_; ++i) {
      delete plates_[i];
    }
    delete[] plates_;
    plates_ = nullptr;
  }
}


glm::vec3 TectonicPlates::GetPlateColor(int index_plate) {
  return plates_[index_plate]->GetColor();
}


glm::vec3 TectonicPlates::GetPlateColorOfRegion(int index_region) {
  Region *r = region_manager_->GetRegion(index_region);
  Plate *p = plates_[r->GetNumPlate()];
  return p->GetColor();
}


std::unordered_set<int> TectonicPlates::GetRegionsOfPlate(int index_plate) {
  return plates_[index_plate]->GetRegions();
}


bool TectonicPlates::RegionIsBorder(int index_region) {
  if (border_regions_.find(index_region) != border_regions_.end())
    return true;
  return false;
}


std::vector<std::pair<IntersectPoint*, IntersectPoint*>>
TectonicPlates::GetBorderPoints() {
  return border_points_;
}

// ======== ANNEXE FUNCTIONS

// Annexe function for construction of TechtonicPlates
// Select random regions from wich we will flood fill the plate
// Each region is assign a plate
// Return the selected regions
std::unordered_set <int> TectonicPlates::SelectStartingRegions() {
  time_t timer;
  uint seed = static_cast<uint>(time(&timer));
  std::unordered_set <int> regions_with_plate;
  int index_chosen_region = -1;
  for (int num_plate = 0; num_plate < number_of_plates_; num_plate++) {
    bool regionPresent = true;
    while (regionPresent) {
      index_chosen_region = rand_r(&seed) % number_of_regions_;
      regionPresent = regions_with_plate.find(index_chosen_region) !=
                                              regions_with_plate.end();
    }
    regions_with_plate.insert(index_chosen_region);
    region_manager_->GetRegion(index_chosen_region)
                   ->SetNumPlateBelong(num_plate);
    plates_[num_plate]->AddRegion(index_chosen_region);
  }
  return regions_with_plate;
}


std::unordered_set <int> GetRegionsWithoutPlate(
    std::unordered_set <int> regions_with_plate, int number_of_regions) {
  std::unordered_set <int> regions_without_plate;
  for (int n = 0; n < number_of_regions; n++) {
    bool regionPresent = regions_with_plate.find(n) != regions_with_plate.end();
    if (!regionPresent) {
      regions_without_plate.insert(n);
    }
  }
  return regions_without_plate;
}


void TectonicPlates::AffectToEachRegionAPlate() {
  std::unordered_set <int> regions_take_neighbors;
  regions_take_neighbors = SelectStartingRegions();

  std::unordered_set <int> regions_without_plate;
  regions_without_plate = GetRegionsWithoutPlate(regions_take_neighbors,
                                                 number_of_regions_);

  std::unordered_set<int> copy;
  while (regions_without_plate.size() != 0) {
    unsigned int size = regions_without_plate.size();
    copy = std::move(regions_take_neighbors);
    for (auto index_chosen_region : copy) {
      Region *region = region_manager_->GetRegion(index_chosen_region);
      for (auto index_neighbor_region : *(region->GetNeigborsRegions())) {
        Region *neighbor_region =
            region_manager_->GetRegion(index_neighbor_region);
        if (neighbor_region->GetNumPlate() == NO_PLATE) {
          int numero_plate = region_manager_->GetRegion(index_chosen_region)
                                            ->GetNumPlate();
          neighbor_region->SetNumPlateBelong(numero_plate);
          plates_[numero_plate]->AddRegion(index_chosen_region);
          regions_without_plate.erase(neighbor_region->GetId());
        }
        regions_take_neighbors.insert(neighbor_region->GetId());
      }
    }
    copy.clear();
    if (size == regions_without_plate.size())
      break;
  }
}


void TectonicPlates::SetBorderRegions() {
  for (int index_r = 0; index_r < number_of_regions_; index_r++) {
    Region *region = region_manager_->GetRegion(index_r);
    for (auto index_neighbor_region : *(region->GetNeigborsRegions())) {
      Region *neighbor_region =
          region_manager_->GetRegion(index_neighbor_region);
      if (neighbor_region->GetNumPlate() != region->GetNumPlate()) {
        border_regions_.insert(index_r);
        break;
      }
    }
  }
}


// Annexe function for SetBorderPoints
// Return the pair of IntersectPoints common bewteen two regions
static std::pair <IntersectPoint*, IntersectPoint*> FindBordersBewteenRegions(
    Region *region1,
    Region *region2) {
  unsigned int nb_ip_found = 0;
  std::pair<IntersectPoint*, IntersectPoint*> border;
  for (int index_1 = 0; index_1 < region1->GetNumberOfIPoints(); index_1++) {
    IntersectPoint *ip1 = region1->GetIPoints()[index_1];
    for (int index_2 = 0; index_2 < region2->GetNumberOfIPoints(); index_2++) {
      IntersectPoint *ip2 = region2->GetIPoints()[index_2];
      if (*ip1 == *ip2) {
        if (nb_ip_found == 0) {
          border.first = ip1;
        } else {
          border.second = ip2;
        }
        nb_ip_found++;
        break;
      }
    }
    if (nb_ip_found == 2) {
      break;
    }
  }
  return border;
}


// Annexe function for construction of TechtonicPlates
void TectonicPlates::SetBorderPoints() {
  for (auto index_br : border_regions_) {
    Region * region = region_manager_->GetRegion(index_br);
    std::unordered_set <int> *neighbors_regions = region->GetNeigborsRegions();
    for (auto index_nr : *(neighbors_regions)) {
      Region * nr_region = region_manager_->GetRegion(index_nr);
      bool part_of_border_regions =
          border_regions_.find(index_nr) != border_regions_.end();
      bool num_plate_different =
          region->GetNumPlate() != nr_region->GetNumPlate();
      if (part_of_border_regions &&  num_plate_different) {
        std::pair <IntersectPoint*, IntersectPoint*> border;
        border = FindBordersBewteenRegions(region, nr_region);
        border_points_.insert(border_points_.begin(), border);
      }
    }
  }
}


// Annexe function for display of TechtonicPlates
void TectonicPlates::DefineColorPlates() {
  std::vector<glm::vec3> plates_colors;
  plates_colors.push_back(glm::vec3(239, 83, 80));    // Red
  plates_colors.push_back(glm::vec3(255, 238, 88));   // Yellow
  plates_colors.push_back(glm::vec3(236, 64, 122));   // Pink
  plates_colors.push_back(glm::vec3(126, 87, 194));   // DeepPurple
  plates_colors.push_back(glm::vec3(66, 165, 245));   // Blue
  plates_colors.push_back(glm::vec3(102, 187, 106));  // Green
  plates_colors.push_back(glm::vec3(92, 107, 192));   // Indigo
  plates_colors.push_back(glm::vec3(171, 71, 188));   // Purple
  plates_colors.push_back(glm::vec3(120, 144, 156));  // BlueGrey
  plates_colors.push_back(glm::vec3(41, 182, 246));   // LightBlue
  plates_colors.push_back(glm::vec3(38, 198, 218));   // Cyan
  plates_colors.push_back(glm::vec3(38, 166, 154));   // Teal
  plates_colors.push_back(glm::vec3(156, 204, 101));  // LightGreen
  plates_colors.push_back(glm::vec3(212, 225, 87));   // Lime
  plates_colors.push_back(glm::vec3(255, 202, 40));   // Amber
  plates_colors.push_back(glm::vec3(255, 167, 38));   // Orange
  plates_colors.push_back(glm::vec3(255, 112, 67));   // DeepOrange
  plates_colors.push_back(glm::vec3(141, 110, 99));   // Brown

  for (int index_p = 0; index_p < number_of_plates_; index_p++) {
    Plate * p = plates_[index_p];
    p->SetColor(plates_colors[index_p]);
  }
}
