// "Copyright 2019 Billy Boys"
#include "parser.h"

#include <algorithm>
#include <fstream>
#include <sstream>
#include <vector>

#include "error_strings.h"


std::vector<std::string> Split(const std::string &str, char delimiter) {
  std::vector<std::string> internal;
  std::stringstream ss(str);
  std::string tok;

  while (std::getline(ss, tok, delimiter)) {
    internal.push_back(tok);
  }
  return internal;
}


bool correctDataInFile(std::vector<std::string> splitted_var) {
  return(splitted_var.at(0)[0] == 't') &&
         (splitted_var.at(1)[0] == 'p') &&
         (splitted_var.at(2)[0] == 'a') &&
         (splitted_var.at(2)[1] == 'l') &&
         (splitted_var.at(2)[2] == 't');
}


int Parser::CountGenerator(const std::string &filename) {
  std::ifstream my_file(filename);
  if (my_file.is_open()) {
    std::string string_to_store;
    int number_generators = 0;
    while (std::getline(my_file, string_to_store)) {
      if (string_to_store[0] == '#' || string_to_store[0] == '\0') {
        continue;
      }
      number_generators++;
    }
    my_file.close();
    return number_generators;
  }
  throw std::invalid_argument(parser_CountGenerator + file404_ERROR);
}


void Parser::ExportGenerators(const std::string &filename,
  Generator **generators_array,
  int number_of_generators) {
    std::ofstream my_file(filename);
    if (my_file) {
      double theta, phi, alt;
      for (int i = 0; i < number_of_generators - 4; ++i) {
        theta = generators_array[i]->GetTheta();
        phi = generators_array[i]->GetPhi();
        alt = generators_array[i]->GetAltitude();
        my_file << "t = " + std::to_string(theta)
        + ", p = " + std::to_string(phi)
        + ", alt = " + std::to_string(alt)
        << std::endl;
      }
      my_file.close();
    } else {
      throw std::runtime_error(openfile_ERROR + filename + "\n");
    }
}


void Parser::LoadGeneratorFromTXT(const std::string &filename,
                                  Generator **generators_array) {
  std::string::size_type sz;
  std::ifstream my_file(filename);

  if (my_file.is_open()) {
    std::string string_to_store;

    my_file.clear();
    my_file.seekg(0, std::ios::beg);
    int counter = 0;

    while (getline(my_file, string_to_store)) {
      if (string_to_store[0] == '#' || string_to_store[0] == '\0') {
        continue;
      }
      std::string::iterator end_pos = std::remove(string_to_store.begin(),
                                              string_to_store.end(), ' ');
      string_to_store.erase(end_pos, string_to_store.end());
      string_to_store = Split(string_to_store, '#')[0];
      std::vector<std::string> splitted_var = Split(string_to_store, ',');

      if (!correctDataInFile(splitted_var)) {
        throw std::invalid_argument(parser_datainput_ERROR);
      }
      double theta = stod (Split(splitted_var[0], '=')[1], &sz);
      double phi = stod (Split(splitted_var[1], '=')[1], &sz);
      double alt = stod (Split(splitted_var[2], '=')[1], &sz);
      generators_array[counter] = new Generator (theta, phi, alt, counter);
      ++counter;
    }
    my_file.close();
  }  else {
    throw std::invalid_argument(parser_LoadGeneratorFromTXT + file404_ERROR);
  }
}
