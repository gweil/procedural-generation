// "Copyright 2019 Billy Boys"
#include "controler.h"

#include <cmath>

#include "intersect_point.h"
#include "region.h"


Controler::Controler(int nb_generators, int nb_plates) {
  voronoi_ = new Voronoi(nb_generators, nb_plates);
  triangles_it_ = voronoi_->GetDelaunay()->GetTriangleSet()->begin();
  number_of_triangles_ = voronoi_->GetDelaunay()->GetTriangleSet()->size();
}


Controler::Controler(char *filename, int nb_plates) {
  voronoi_ = new Voronoi(filename, nb_plates);
  triangles_it_ = voronoi_->GetDelaunay()->GetTriangleSet()->begin();
  number_of_triangles_ = voronoi_->GetDelaunay()->GetTriangleSet()->size();
}


Controler::~Controler() {
  delete voronoi_;
}


void Controler::EventOnPress(unsigned char event[3]) {
  if ((event[0] == 2 && event[1] == 2 && event[2] == 2) ||
      (event[0] == 252 && event[1] == 252 && event[2] == 252)) {
    voronoi_->GetRegionManager()->DecTemperature();
  } else if ((event[0] == 5 && event[1] == 5 && event[2] == 5) ||
             (event[0] == 242 && event[1] == 242 && event[2] == 242)) {
    voronoi_->GetRegionManager()->IncTemperature();
  } else if ((event[0] == 8 && event[1] == 8 && event[2] == 8) ||
             (event[0] == 250 && event[1] == 250 && event[2] == 250)) {
    voronoi_->GetRegionManager()->DecMoisture();
  } else if ((event[0] == 10 && event[1] == 10 && event[2] == 10) ||
             (event[0] == 240 && event[1] == 240 && event[2] == 240)) {
    voronoi_->GetRegionManager()->IncMoisture();
  } else if ((event[0] == 15 && event[1] == 15 && event[2] == 15) ||
             (event[0] == 247 && event[1] == 247 && event[2] == 247)) {
    voronoi_->GetRegionManager()->DecWaterLevel();
  } else if ((event[0] == 13 && event[1] == 13 && event[2] == 13) ||
             (event[0] == 237 && event[1] == 237 && event[2] == 237)) {
    voronoi_->GetRegionManager()->IncWaterLevel();
  } else if ((event[0] == 18 && event[1] == 18 && event[2] == 18) ||
             (event[0] == 245 && event[1] == 245 && event[2] == 245)) {
    voronoi_->GetRegionManager()->IncrementOctaves();
  } else if ((event[0] == 20 && event[1] == 20 && event[2] == 20) ||
             (event[0] == 235 && event[1] == 235 && event[2] == 235)) {
    voronoi_->GetRegionManager()->DecrementOctaves();
  }
}


bool Controler::VerifyVoronoiDiagram() {
  return voronoi_->VerifyVoronoiDiagram();
}


void Controler::ExportDiagram() {
  Parser::ExportGenerators("exportGenerator.txt",
                           voronoi_->GetDelaunay()->GetGeneratorArray(),
                           voronoi_->GetNumberGenerator());
}


glm::vec3 Controler::CoordIPoint(uint index_r, uint index_i) {
  int nb_of_regions = voronoi_->GetRegionManager()->GetNumberRegion();
  if (index_r < static_cast<uint>(nb_of_regions)) {
    Region *region = voronoi_->GetRegionManager()->GetRegion(index_r);
    int nb_of_ipoints = region->GetNumberOfIPoints();
    if (index_i < static_cast<uint>(nb_of_ipoints)) {
      IntersectPoint *i = region->GetIPoints()[index_i];
      if (i == nullptr)
        return glm::vec3(NAN, NAN, NAN);
      return glm::vec3(i->x(), i->y(), i->z());
    }
  }
  return glm::vec3(NAN, NAN, NAN);
}


bool Controler::RegionIndexIsValid(int index_region) {
  if (voronoi_->GetRegionManager()->GetRegion(index_region) == nullptr)
    return false;
  return true;
}


bool Controler::RegionIsBorder(int index_region) {
  return voronoi_->GetTectonicPlates()->RegionIsBorder(index_region);
}


glm::vec3 Controler::GetRegionColor(int index_region) {
  return voronoi_->GetRegionManager()->GetRegionColor(index_region);
}


int Controler::GetNumberGenerator() {
  return voronoi_->GetNumberGenerator();
}


glm::vec3 Controler::GetPlateColorOfRegion(int index_region) {
  return voronoi_->GetTectonicPlates()->GetPlateColorOfRegion(index_region);
}


std::vector<std::pair<IntersectPoint*, IntersectPoint*>>
Controler::GetBorderPoints() {
  return voronoi_->GetTectonicPlates()->GetBorderPoints();
}


int Controler::GetRegionDownHillIndex(int index_region) {
  return voronoi_->GetRegionManager()->GetRegion(index_region)->
         GetDownHillRegion();
}


int Controler::GetRegionLandscape(int index_region) {
  return voronoi_->GetRegionManager()->GetRegion(index_region)->
         GetLandscapeType();
}


float Controler::GetRegionAltitude(int index_region) {
  return voronoi_->GetRegionManager()->GetRegion(index_region)->
         GetGenerator()->GetAltitude();
}


int Controler::GetPerlinOctaves() {
  return voronoi_->GetRegionManager()->GetPerlinOctaves();
}


float Controler::GetSeaLevel() {
  return voronoi_->GetRegionManager()->GetSeaLevel();
}


Region *Controler::GetRegion(int index_region) {
  return voronoi_->GetRegionManager()->GetRegion(index_region);
}


std::unordered_set <int> Controler::GetSelectedRivers() {
  return voronoi_->GetRegionManager()->GetRivers()->GetSelectedRivers();
}


glm::vec3 **Controler::GetRiversSegmentations() {
  return voronoi_->GetRegionManager()->GetRivers()->GetSegmentations();
}


int Controler::GetRiversSegmentDivisions() {
  return voronoi_->GetRegionManager()->GetRivers()->GetDiv();
}


int Controler::GetNbTriangles() {
  return number_of_triangles_;
}


int Controler::GetTemperature() {
  return voronoi_->GetRegionManager()->GetTemperature();
}


int Controler::GetMoisture() {
  return voronoi_->GetRegionManager()->GetMoisture();
}


std::array<glm::vec3, 3> Controler::GetTrianglePoints(int counter) {
  std::unordered_set<Triangle*>::iterator it_tmp = triangles_it_;
  while (counter != 0) {
    it_tmp++;
    counter--;
  }
  std::array<glm::vec3, 3> points;
  Triangle *triangle = *it_tmp;
  Generator *generator0 = triangle->GetGenerator(0);
  Generator *generator1 = triangle->GetGenerator(1);
  Generator *generator2 = triangle->GetGenerator(2);
  points[0] = generator0->GetCartesianCoord();
  points[1] = generator1->GetCartesianCoord();
  points[2] = generator2->GetCartesianCoord();
  return points;
}
