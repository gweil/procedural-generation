// "Copyright 2019 Billy Boys"
#include "voronoi.h"

#include "error_strings.h"


// Annexe function
void SetNeighborsRegions(Region **regions, int number_of_regions) {
  for (int index_r = 0; index_r < number_of_regions; index_r++) {
    Generator *generator = regions[index_r]->GetGenerator();
    std::unordered_set <Triangle*,
                      std::TriangleHasher,
                      std::TriangleComp> triangles =
      generator->GetBelongingTriangles();
    for (Triangle *t : triangles) {
      for (int indexG = 0; indexG < 3; indexG ++) {
        Generator *g = t->GetGenerator(indexG);
        regions[index_r]->GetNeigborsRegions()->insert(g->GetId());
      }
    }
    regions[index_r]->GetNeigborsRegions()->erase(generator->GetId());
  }
}


Voronoi::Voronoi(const std::string &filename, const int nb_plates) {
  delaunay_ = new Delaunay(filename);
  number_of_generators_ = delaunay_->GetNumberGenerator();
  if (nb_plates > number_of_generators_/10)
    throw std::invalid_argument(voronoi_datainput_ERROR
                                + number_of_plates_ERROR);
  if (nb_plates > 18)
    throw std::invalid_argument(techtonicplates_datainput_ERROR);

  VerifyVoronoiDiagram();
  regions_ = GenerateRegion();

  region_manager_ = new RegionManager(regions_, number_of_generators_);
  tectonic_plates_ = new TectonicPlates(nb_plates, number_of_generators_,
                                        region_manager_);
}


Voronoi::Voronoi(const int number_of_generator, const int nb_plates) {
  if (number_of_generator <= 0)
    throw std::invalid_argument(voronoi_datainput_ERROR
                                + number_of_generators_ERROR);
  if (nb_plates > number_of_generator/10)
    throw std::invalid_argument(voronoi_datainput_ERROR
                                + number_of_plates_ERROR);
  if (nb_plates > 18)
    throw std::invalid_argument(techtonicplates_datainput_ERROR);

  delaunay_ = new Delaunay(number_of_generator + 4);
  number_of_generators_ = delaunay_->GetNumberGenerator();

  VerifyVoronoiDiagram();

  regions_ = GenerateRegion();

  region_manager_ = new RegionManager(regions_, number_of_generators_);
  tectonic_plates_ = new TectonicPlates(nb_plates, number_of_generators_,
                                        region_manager_);
}


Voronoi::~Voronoi() {
  delete delaunay_;
  delete tectonic_plates_;
  for (int i = 0; i < number_of_generators_; i++) {
    delete regions_[i];
  }
  delete[] regions_;
  delete region_manager_;
}


Region **Voronoi::GenerateRegion() {
  Region **regions = new Region *[number_of_generators_];
  for (int index = 0; index < number_of_generators_; index++) {
    Generator *generator = delaunay_->GetGeneratorArray()[index];
    regions[index] = new Region(generator);
  }
  SetNeighborsRegions(regions, number_of_generators_);
  return regions;
}


bool Voronoi::VerifyVoronoiDiagram() {
  if (delaunay_->VerifyGeneratorWithoutTriangle()) {
    throw std::invalid_argument(delaunay_triangulation_ERROR);
  }

  return delaunay_->VerifyDelaunayTriangulation();
}
