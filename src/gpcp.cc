// "Copyright 2019 Billy Boys"
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <cmath>
#include <cstring>
#include <iostream>

#include "controler.h"

#define NB_PLATES 18

// -------- Global variables --------

GLfloat auto_rotation_angle = 0.0f;
int refresh_milliseconds = 15;
static float scale = 1.0;
int zoomLevel = 100;
static float rot_x = 1.0, rot_y = 1.0;
Controler *controler = nullptr;
bool display_delaunay = false;
bool display_voronoi = true;
bool display_plates = false;
bool display_rivers = false;
bool no_display = false;
void *font = GLUT_BITMAP_9_BY_15;
char *planet_name = nullptr;
bool name_generated = false;
int window_width, window_height;
unsigned char color_pick[3];
int temperature = 3, moisture = 3, landRatio = 60;

// -------- Functions --------

glm::vec3 Darken(glm::vec3 color) {
  float darkCoeff = 10.0f;
  return glm::vec3(fmax(0, color[0] - darkCoeff),
                   fmax(0, color[1] - darkCoeff),
                   fmax(0, color[2] - darkCoeff));
}

static void DrawBorderPoint(glm::vec3 color, float width_line,
    std::vector<std::pair<IntersectPoint *, IntersectPoint *>> border_points) {
  glColor3f(color.x, color.y, color.z);
  glLineWidth(width_line);
  for (auto pair_ip : border_points) {
    // Avoid segfault when Voronoi construction is incorrect
    if (pair_ip.first != nullptr && pair_ip.second != nullptr) {
      glBegin(GL_LINES);
      glVertex3f(pair_ip.first->x(), pair_ip.first->y(), pair_ip.first->z());
      glVertex3f(pair_ip.second->x(), pair_ip.second->y(), pair_ip.second->z());
      glEnd();
    }
  }
}

void DisplayVoronoi() {
  int index_region = 0;
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

  while (controler->RegionIndexIsValid(index_region)) {
    glm::vec3 color = controler->GetRegionColor(index_region);
    glColor3f(color.x, color.y, color.z);
    glBegin(GL_POLYGON);
    int index_intersect = 0;
    while (1) {
      glm::vec3 pt = controler->CoordIPoint(index_region, index_intersect);
      if (std::isnan(pt.x) && std::isnan(pt.y) && std::isnan(pt.z))
        break;
      glVertex3f(pt.x, pt.y, pt.z);
      index_intersect++;
    }
    glEnd();
    index_region++;
  }
}

void DisplayRivers() {
  int index_selected_river = 0;
  for (int generator_number : controler->GetSelectedRivers()) {
    int region_1 = generator_number;
    glm::vec3 color;
    while (1) {
      int region_2 = controler->GetRegionDownHillIndex(region_1);
      if (controler->GetRegionLandscape(region_2) == 1) {
        float alt;
        alt = controler->GetRegionAltitude(region_2);
        color = glm::vec3(0, 0, 1 - fabs(alt));
        break;
      } else {
        region_1 = region_2;
      }
    }
    region_1 = generator_number;

    while (1) {
      int region_2 = controler->GetRegionDownHillIndex(region_1);

      for (int i = 0; i < controler->GetRiversSegmentDivisions() - 1; i++) {
        glm::vec3 **segmentations = controler->GetRiversSegmentations();
        glm::vec3 p0 = segmentations[index_selected_river][i];
        glm::vec3 p1 = segmentations[index_selected_river][i + 1];
        Tools::PrintArc(p0, p1, 1.0f, color,
                        controler->GetRiversSegmentDivisions()/3+1);
      }
      if (controler->GetRegionLandscape(region_2) == 1) {
        break;
      } else {
        region_1 = region_2;
      }
    }
    index_selected_river++;
  }
}

void DisplayPlates() {
  int index_region = 0;
  glLineWidth(1);
  while (controler->RegionIndexIsValid(index_region)) {
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glm::vec3 color = controler->GetPlateColorOfRegion(index_region);
    if (controler->RegionIsBorder(index_region))
      color = Darken(color);
    glColor3f(color[0] / 255.0f, color[1] / 255.0f , color[2] / 255.0f);
    glBegin(GL_POLYGON);
    int index_intersect = 0;
    while (1) {
      glm::vec3 pt = controler->CoordIPoint(index_region, index_intersect);
      if (std::isnan(pt.x) && std::isnan(pt.y) && std::isnan(pt.z))
        break;
      glVertex3f(pt.x, pt.y, pt.z);
      index_intersect++;
    }
    glEnd();
    index_region++;
  }
  glm::vec3 color = glm::vec3(1, 1, 1);
  DrawBorderPoint(color, 3, controler->GetBorderPoints());
}

void DisplayDelaunay() {
  glLineWidth(1);
  for (int triangle = 0; triangle < controler->GetNbTriangles(); ++triangle) {
    std::array<glm::vec3, 3> points = controler->GetTrianglePoints(triangle);
    glColor3f(0.8, 0.0, 0.5);
    Tools::CreateLine(points[0], points[1]);
    Tools::CreateLine(points[0], points[2]);
    Tools::CreateLine(points[2], points[1]);
  }
}

void Displays() {
  glColor3f(1.0, 1.0, 1.0);
  if (display_voronoi) {
    DisplayVoronoi();
    if (display_rivers)
      DisplayRivers();
  } else if (display_plates) {
    DisplayPlates();
  } else if (display_rivers) {
    DisplayVoronoi();
    DisplayRivers();
  }
  if (display_delaunay)
    DisplayDelaunay();
  glLoadIdentity();
  glTranslatef(0.0, 0.0, -5.0);
}

void output(double x, double y, const char *string) {
  int len, i;
  glRasterPos2d(x, y);
  len = static_cast<int>(strlen(string));
  for (i = 0; i < len; i++) {
    glutBitmapCharacter(font, string[i]);
  }
}

void DisplayHelp() {
  glColor3f(0.0, 0.0, 0.0);
  std::string help = "COMMANDS MENU";
  output(-2.85, -1.65, help.c_str());
  help = "Z / Q / S / D : rotation";
  output(-3.1, -1.75, help.c_str());
  help = "        + / - : zoom / unzoom";
  output(-3.1, -1.85, help.c_str());
  help = "       Escape : exit";
  output(-3.1, -1.95, help.c_str());
}

void DisplayInfo() {
  glColor3f(0.0, 0.0, 0.0);
  std::string name = planet_name;
  std::string info = "ABOUT";
  output(2.6, -1.65, info.c_str());
  info = "Number of generators : ";
  info += std::to_string(controler->GetNumberGenerator());
  output(1.95, -1.75, info.c_str());
  info = "                Zoom : " + std::to_string(zoomLevel) + "%";
  output(1.95, -1.85, info.c_str());
  info = "                Name : " + name;
  output(1.95, -1.95, info.c_str());
}

void CreateButton(float x, float x2, float y, float y2, glm::vec3 color) {
  glColor3f(color.x, color.y, color.z);
  glBegin(GL_QUADS);
  glVertex3f(x, y, -0.09f);
  glVertex3f(x, y2, -0.09f);
  glVertex3f(x2, y2, -0.09f);
  glVertex3f(x2, y, -0.09f);
  glEnd();
}

void DisplayControlPanel() {
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  CreateButton(-1.9f, -1.75f, 1.76f, 1.83f, glm::vec3(0.01));
  CreateButton(-1.7f, -1.55f, 1.76f, 1.83f, glm::vec3(0.02));
  CreateButton(-1.9f, -1.75f, 1.66f, 1.73f, glm::vec3(0.03));
  CreateButton(-1.7f, -1.55f, 1.66f, 1.73f, glm::vec3(0.04));
  CreateButton(-1.7f, -1.55f, 1.56f, 1.63f, glm::vec3(0.05));
  CreateButton(-1.9f, -1.75f, 1.56f, 1.63f, glm::vec3(0.06));
  CreateButton(-1.7f, -1.55f, 1.46f, 1.53f, glm::vec3(0.07));
  CreateButton(-1.9f, -1.75f, 1.46f, 1.53f, glm::vec3(0.08));

  glColor3f(0.0, 0.0, 0.0);
  std::string info = "CONTROL PANEL";
  output(-2.8, 1.85, info.c_str());
  info += std::to_string(controler->GetTemperature());
  info = "      Temperature : ";
  temperature = controler->GetTemperature();
  switch (temperature) {
    case 0:
      info += "Frosty";
      break;
    case 1:
      info += "Cold";
      break;
    case 2:
      info += "Temperate";
      break;
    case 3:
      info += "Hot";
      break;
    case 4:
      info += "Torrid";
      break;
    default:
      break;
  }
  output(-3.1, 1.75, info.c_str());
  info = "         Moisture : ";
  moisture = controler->GetMoisture();
  switch (moisture) {
    case 0:
      info += "Superarid";
      break;
    case 1:
      info += "Arid";
      break;
    case 2:
      info += "Normal";
      break;
    case 3:
      info += "Humid";
      break;
    case 4:
      info += "Superhumid";
      break;
    default:
      break;
  }
  output(-3.1, 1.65, info.c_str());
  info = "        Sea level : "+std::to_string(controler->GetSeaLevel());
  output(-3.1, 1.55, info.c_str());
  info = "Number of octaves : "+std::to_string(controler->GetPerlinOctaves());
  output(-3.1, 1.45, info.c_str());
  glColor3f(1.0, 1.0, 1.0);
  info = "-";
  glColor3f(0.99, 0.99, 0.99);
  output(-1.80f, 1.75f, info.c_str());
  glColor3f(0.98, 0.98, 0.98);
  output(-1.80f, 1.65f, info.c_str());
  glColor3f(0.97, 0.97, 0.97);
  output(-1.80f, 1.55f, info.c_str());
  glColor3f(0.96, 0.96, 0.96);
  output(-1.80f, 1.45f, info.c_str());
  info = "+";
  glColor3f(0.95, 0.95, 0.95);
  output(-1.61f, 1.75f, info.c_str());
  glColor3f(0.94, 0.94, 0.94);
  output(-1.61f, 1.65f, info.c_str());
  glColor3f(0.93, 0.93, 0.93);
  output(-1.61f, 1.55f, info.c_str());
  glColor3f(0.92, 0.92, 0.92);
  output(-1.61f, 1.45f, info.c_str());
}

void DisplayStats() {
  float xStat1 = -3.2f;
  float xStat2 = -3.0f;
  float xStat3 = -2.9f;

  glColor3f(0.02, 0.02, 0.02);
  std::string stats = "BIOMES";
  output(-2.85, 0.8, stats.c_str());

  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  CreateButton(xStat1, xStat2, 0.6f, 0.7f, glm::vec3(0.88, 0.96, 0.99));
  CreateButton(xStat1, xStat2, 0.45f, 0.55f, glm::vec3(0.50, 0.84, 1.0));
  CreateButton(xStat1, xStat2, 0.3f, 0.4f, glm::vec3(0.11, 0.53, 0.89));
  CreateButton(xStat1, xStat2, 0.25f, 0.15f, glm::vec3(0.05, 0.27, 0.63));
  CreateButton(xStat1, xStat2, 0.f, 0.1f, glm::vec3(0.0, 0.53, 0.48));
  CreateButton(xStat1, xStat2, -0.15f, -0.05f, glm::vec3(0.29, 0.68, 0.31));
  CreateButton(xStat1, xStat2, -0.2f, -0.3f, glm::vec3(0.33, 0.54, 0.18));
  CreateButton(xStat1, xStat2, -0.35f, -0.45f, glm::vec3(0.98, 0.75, 0.17));
  CreateButton(xStat1, xStat2, -0.5f, -0.6f, glm::vec3(1.0, 0.96, 0.46));

  glColor3f(0.00, 0.00, 0.00);
  stats = "Polar Desert";
  output(xStat3, 0.62, stats.c_str());
  stats = "Taiga";
  output(xStat3, 0.47, stats.c_str());
  stats = "Ocean";
  output(xStat3, 0.32, stats.c_str());
  stats = "Deep Ocean";
  output(xStat3, 0.17, stats.c_str());
  stats = "Taiga";
  output(xStat3, 0.02, stats.c_str());
  stats = "Temperate Deciduous Forest";
  output(xStat3, -0.13, stats.c_str());
  stats = "Tropical Rain Forest";
  output(xStat3, -0.28, stats.c_str());
  stats = "Savanna";
  output(xStat3, -0.42, stats.c_str());
  stats = "Desert";
  output(xStat3, -0.57, stats.c_str());
}

void Display() {
  glClearColor(1.0, 1.0, 1.0, 1.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);

  glPushMatrix();
  glRotatef(rot_x, 1.0, 0.0, 0.0);
  glRotatef(rot_y, 0.0, 1.0, 0.0);
  Displays();

  DisplayHelp();
  DisplayInfo();
  DisplayControlPanel();
  DisplayStats();

  glScalef(scale, scale, scale);
  glutSwapBuffers();
}

void Timer(int value) {
  (void) value;
  glutPostRedisplay();
  glutTimerFunc(refresh_milliseconds, Timer, 0);
}

void Reshape(GLsizei width, GLsizei height) {
  if (height == 0) {
    height = 1;
  }
  GLfloat aspect = static_cast<GLfloat>(width) / static_cast<GLfloat>(height);
  window_height = height;
  glViewport(0, 0, width, height);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0f, aspect, 0.1f, 100.0f);
}

void KeyPressed(unsigned char key, int x, int y) {
  (void) x;
  (void) y;
  if (key == 27) {
    glutLeaveMainLoop();
  }
  if (key == 'q') {
    rot_y -= 4.0f;
  }
  if (key == 'd') {
    rot_y += 4.0f;
  }
  if (key == 'z') {
    rot_x -= 4.0f;
  }
  if (key == 's') {
    rot_x += 4.0f;
  }
  if (key == '+') {
    scale = std::min(3.0f, scale + 0.2f);
    if (zoomLevel < 300) {
      zoomLevel += 20;
    }
  }
  if (key == '-') {
    scale = std::max(0.2f, scale - 0.2f);
    if (zoomLevel > 20) {
      zoomLevel -= 20;
    }
  }
}

static void Mouse(int button, int state, int mouse_x, int mouse_y) {
  (void) button;
  if (state == GLUT_DOWN) {
    glReadPixels(mouse_x, window_height - mouse_y, 1, 1, GL_RGB,
                 GL_UNSIGNED_BYTE, color_pick);
    controler->EventOnPress(color_pick);
  }
}

void ProcessMenuEvents(int option) {
  switch (option) {
    case (0):
      display_rivers = !display_rivers;
      if (display_rivers == true)
        display_voronoi = true;
      display_plates = false;
      break;
    case (1):
      display_voronoi = !display_voronoi;
      if (display_voronoi == false)
        display_rivers = false;
      display_plates = false;
      break;
    case (2):
      display_delaunay = !display_delaunay;
      break;
    case (3):
      display_plates = !display_plates;
      display_voronoi = false;
      display_rivers = false;
      break;
    default:
      break;
  }
}

void CreateGLUTMenus() {
  glutCreateMenu(ProcessMenuEvents);
  glutAddMenuEntry("Show/Hide Rivers", 0);
  glutAddMenuEntry("Show/Hide Voronoi", 1);
  glutAddMenuEntry("Show/Hide Delaunay", 2);
  glutAddMenuEntry("Show/Hide Plates", 3);
  glutAttachMenu(GLUT_RIGHT_BUTTON);
}

int main(int argc, char *argv[]) {
  if (argc == 2) {
    if (strstr(argv[1], ".txt") != nullptr) {
      controler = new Controler(argv[1], NB_PLATES);
    } else if (Tools::IsNumber(argv[1])) {
      controler = new Controler(atoi(argv[1]), NB_PLATES);
    } else {
      throw std::invalid_argument("Error : generators file doesnt exist");
    }
  } else if (argc == 3) {
    for (int arg = 1; arg < argc; ++arg) {
      if (strstr(argv[arg], ".txt") != nullptr) {
        controler = new Controler(argv[arg], NB_PLATES);
      } else if (Tools::IsNumber(argv[arg])) {
        controler = new Controler(atoi(argv[arg]), NB_PLATES);
      } else if (strstr(argv[arg], "-nodisplay") != nullptr) {
        no_display = true;
      } else {
        throw std::invalid_argument("Error : generators file doesnt exist");
      }
    }
  } else {
    std::string errmsg = "Error : Specify filename or number of generators";
    throw std::invalid_argument(errmsg.c_str());
  }

  if (controler->VerifyVoronoiDiagram()) {
    std::cout << "Correct construction of voronoi diagram" << std::endl;
    controler->ExportDiagram();
  } else {
    std::cout << "Incorrect construction of voronoi diagram" << std::endl;
  }

  if (!no_display) {
    std::string name = Tools::NameGen();
    planet_name = &name[0u];
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE);
    window_width = glutGet(GLUT_SCREEN_WIDTH) * 0.8;
    if (window_width > 2000) {
      window_width = window_width / 2;
    }
    window_height = glutGet(GLUT_SCREEN_HEIGHT) * 0.8;
    glutInitWindowSize(window_width, window_height);
    glutInitWindowPosition(50, 50);
    glutCreateWindow("GPCP");
    CreateGLUTMenus();
    glutDisplayFunc(Display);
    glutReshapeFunc(Reshape);
    glutKeyboardFunc(KeyPressed);
    glutMouseFunc(Mouse);
    glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION);
    glEnable(GL_DEPTH_TEST);
    glutTimerFunc(0, Timer, 0);
    glutMainLoop();
  }

  delete controler;
  return EXIT_SUCCESS;
}
