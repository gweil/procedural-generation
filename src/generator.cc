// "Copyright 2019 Billy Boys"
#include "generator.h"

#include <cmath>


// Constructor with polar coordinates
Generator::Generator(double theta, double phi, double altitude,
                     unsigned int id) : Point() {
  phi_ = phi;
  theta_ = theta;
  altitude_ = altitude;
  id_ = id;
  SetCartesianCoord();
}


// Constructor with cartesian coordinates
Generator::Generator(glm::vec3 coordinates, double altitude,
    unsigned int id) : Point() {
  x_ = coordinates[0];
  y_ = coordinates[1];
  z_ = coordinates[2];
  id_ = id;

  altitude_ = altitude;
  SetPolarCoord();
}


Generator::~Generator() = default;


// We add only if there is no corresponding triangle
// (same ID or sames vertices)
void Generator::AddTriangle(Triangle *triangle) {
    belonging_triangles_.insert(triangle);
}


void Generator::RemoveTriangle(Triangle *triangle) {
  belonging_triangles_.erase(triangle);
}


void Generator::SetCartesianCoord() {
  x_ = RADIUS * static_cast<double> (sin(theta_) * cos(phi_));
  y_ = RADIUS * static_cast<double> (sin(theta_) * sin(phi_));
  z_ = RADIUS * static_cast<double> (cos(theta_));
}


void Generator::SetPolarCoord() {
  theta_ = acos(z() / sqrt(x() * x() + y() * y() + z() * z()));
  phi_ = atan2(y(), x());
  if (phi_ < 0) {
    phi_ += 2*M_PI;
  }
}
