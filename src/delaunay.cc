// "Copyright 2019 Billy Boys"
#include "delaunay.h"

#include <ctime>
#include <random>

#include "error_strings.h"


Delaunay::Delaunay(const std::string &filename) {
  number_of_generators_ = Parser::CountGenerator(filename) + 4;
  // We store 4 more generators for the tetrahedron
  generator_array_ = new Generator*[number_of_generators_];
  Parser::LoadGeneratorFromTXT(filename, generator_array_);
  ConstructDelaunayTriangulation();
}


Delaunay::Delaunay(const int number_of_generator) {
  GenerateRandUniform(number_of_generator);
  ConstructDelaunayTriangulation();
}


Delaunay::~Delaunay() {
  // Free memory of triangles_set_final_
  for (auto it = triangles_set_final_.begin(); it != triangles_set_final_.end();
      ++it) {
    delete *it;
  }
  // Free memory of generator_array_
  if (generator_array_ != nullptr) {
    for (int i = 0; i < number_of_generators_; ++i) {
      delete generator_array_[i];
    }
    delete[] generator_array_;
  }
}


// Annexe function for Verifying Delaunay Triangulation
Triangle *FindOtherCommonTriangle(Triangle *triangle,
                                  Generator *generator0,
                                  Generator *generator1) {
  if (!triangle->IsGeneratorInTriangle(generator0) ||
      !triangle->IsGeneratorInTriangle(generator1)) {
    throw std::invalid_argument(delaunay_FindOtherCommonTriangle_ERROR);
  }
  std::unordered_set <Triangle*,
                      std::TriangleHasher,
                      std::TriangleComp> us_generator0 =
      generator0->GetBelongingTriangles();
  std::unordered_set <Triangle*,
                      std::TriangleHasher,
                      std::TriangleComp> us_generator1 =
      generator1->GetBelongingTriangles();

  for (Triangle *t : us_generator0) {
    if ((us_generator1.find(t)) != us_generator1.end()) {
      Triangle *neighbor = *(us_generator1.find(t));
      // If we find a triangle, we verify that it's not the one we already have
      if (!(*neighbor == *triangle)) {
        return neighbor;
      }
    }
  }
  // It should have another common triangle
  // But sometimes due to random generation of points, it doesn't
  return nullptr;
}


// If we find two generator which don't have two common triangles
// the delaunay triangulation is wrong
bool Delaunay::VerifyDelaunayTriangulation() {
  auto tset_iterator = triangles_set_final_.begin();
  while (tset_iterator != triangles_set_final_.end()) {
    Triangle *triangle = *tset_iterator;
    Generator *generator0 = triangle->GetGenerator(0);
    Generator *generator1 = triangle->GetGenerator(1);
    Generator *generator2 = triangle->GetGenerator(2);

    Triangle *neighbor_s0_s1 = FindOtherCommonTriangle(triangle, generator0,
                                                       generator1);
    Triangle *neighbor_s0_s2 = FindOtherCommonTriangle(triangle, generator0,
                                                       generator2);
    Triangle *neighbor_s1_s2 = FindOtherCommonTriangle(triangle, generator1,
                                                       generator2);
    if (neighbor_s0_s1 != nullptr && neighbor_s0_s2 != nullptr &&
        neighbor_s1_s2 != nullptr) {
      // DO NOTHING
    } else {
      return false;
    }
    ++tset_iterator;
  }
  return true;
}


// If we find one generator which don't belong to at least one triangle
// the delaunay triangulation is wrong
bool Delaunay::VerifyGeneratorWithoutTriangle() {
  for (int index = 0; index < number_of_generators_; index++) {
    if (GetGeneratorArray()[index]->GetNumberBelongingTriangles() == 0) {
      return true;
    }
  }
  return false;
}


// http://corysimon.github.io/articles/uniformdistn-on-sphere/
void Delaunay::GenerateRandUniform(int number) {
  if (number <  1) {
    throw std::invalid_argument(delaunay_GenerateAleatUniform_ERROR);
  }
  generator_array_ = new Generator*[number];
  std::default_random_engine random;
  std::normal_distribution<double> distribution(5.0, 2.0);

  time_t timer;
  uint seed = static_cast<uint>(time(&timer));
  std::mt19937 generator(seed);
  std::uniform_real_distribution<double> uniform01(0.0, 1.0);

  for (int i = 0; i < number - 4; i++) {
    double phi = 2 * M_PI * uniform01(generator);
    double theta = acos(1 - 2 * uniform01(generator));
    double x = sin(theta) * cos(phi);
    double y = sin(theta) * sin(phi);
    double z = cos(theta);

    generator_array_[i] = new Generator(glm::dvec3(x, y, z), 1.0f, i);
  }
  number_of_generators_ = number;
}


// Annexe function for delaunay triangulation
void AddNeighbor(Triangle * triangle,
             std::unordered_set <Generator *> *generators_neighbors_to_check) {
  for (int i = 0; i < 3; i++) {
    generators_neighbors_to_check->insert(triangle->GetGenerator(i));
  }
}


// Annexe function for delaunay triangulation
// Create three triangles around one generator
void TriangulationFromGenerator(Generator *generator, const Triangle *triangle,
                        std::unordered_set <Triangle *> *triangles_to_check) {
  auto triangle1 = new Triangle (triangle->GetGenerator(0),
                                 triangle->GetGenerator(1),
                                 generator);
  auto triangle2 = new Triangle (triangle->GetGenerator(0),
                                 triangle->GetGenerator(2),
                                 generator);
  auto triangle3 = new Triangle (triangle->GetGenerator(1),
                                 triangle->GetGenerator(2),
                                 generator);
  triangles_to_check->insert(triangle1);
  triangles_to_check->insert(triangle2);
  triangles_to_check->insert(triangle3);
}


// Annexe function for delaunay triangulation
// Create the tetrahedron that contains all the generators of the sphere
std::unordered_set <Triangle *> CreateTetraedron(double radius,
                                                 int nb_generators,
                                                 Generator **generator_array) {
  double altitude = 1.0f;
  // Seeds of tetrahedron
  Generator *generator_1 = new Generator(glm::dvec3( 0, 0, 1.0),
                                         altitude, nb_generators - 4);
  Generator *generator_2 = new Generator(glm::dvec3((-sqrt(2.0)/3.0)*radius,
                                         -sqrtf(6.0)/3*radius,
                                         static_cast<float>((-1.0/3.0)*radius)),
                                         altitude, nb_generators - 3);
  Generator *generator_3 = new Generator(glm::dvec3((-sqrt(2.0)/3.0)*radius,
                                         sqrt(6.0)/3*radius,
                                         (-1.0/3.0)*radius),
                                         altitude,
                                         nb_generators - 2);
  Generator *generator_4 = new Generator(glm::dvec3(((2*sqrt(2.0))/3.0)*radius,
                                         0.0, (-1.0/3.0)*radius),
                                         altitude,
                                         nb_generators - 1);

  generator_array[nb_generators - 4] = generator_1;
  generator_array[nb_generators - 3] = generator_2;
  generator_array[nb_generators - 2] = generator_3;
  generator_array[nb_generators - 1] = generator_4;

  std::unordered_set <Triangle *> tetrahedron;

  Triangle *triangle_1 = new Triangle(generator_1, generator_2, generator_3);
  tetrahedron.insert(triangle_1);
  Triangle *triangle_2 = new Triangle(generator_1, generator_2, generator_4);
  tetrahedron.insert(triangle_2);
  Triangle *triangle_3 = new Triangle(generator_1, generator_3, generator_4);
  tetrahedron.insert(triangle_3);
  Triangle *triangle_4 = new Triangle(generator_2, generator_3, generator_4);
  tetrahedron.insert(triangle_4);

  return tetrahedron;
}


void Delaunay::ConstructDelaunayTriangulation() {
  // Initialise supertriangle (tetrahedron)

  std::unordered_set <Triangle *> triangles_set;
  triangles_set = CreateTetraedron(1, number_of_generators_, generator_array_);

  std::unordered_set <Triangle *> triangles_to_check;

  std::unordered_set <Generator *> generators_to_check;

  for (int i = 0; i < number_of_generators_; i++) {
    Generator* generator = generator_array_[i];

    // Create new triangles following delaunays laws
    auto tset_iterator = triangles_set.begin();
    while (tset_iterator != triangles_set.end()) {
      Triangle * triangle = *tset_iterator;
      if (triangle->InCircumscribedSphere(generator)) {
        TriangulationFromGenerator(generator, triangle, &triangles_to_check);
        AddNeighbor(triangle, &generators_to_check);
        tset_iterator = triangles_set.erase(tset_iterator);
        delete triangle;
        triangle = nullptr;
      } else {
        ++tset_iterator;
      }
    }

    // Verify the new triangles are correct
    auto tcheck_iterator = triangles_to_check.begin();
    while (tcheck_iterator != triangles_to_check.end()) {
      Triangle * triangle = *tcheck_iterator;
      int erased = 0;
      for (auto generator_iter : generators_to_check) {
        if (triangle->InCircumscribedSphere(generator_iter)) {
          tcheck_iterator = triangles_to_check.erase(tcheck_iterator);
          delete triangle;
          triangle = nullptr;
          erased = 1;
          break;
        }
      }
      if (erased == 0) {
        ++tcheck_iterator;
      }
    }
    // Insert the rest of the triangles to the main set
    auto tinsert_iterator = triangles_to_check.begin();
    while (tinsert_iterator != triangles_to_check.end()) {
      Triangle * triangle = *tinsert_iterator;
      triangles_set.insert(triangle);
      ++tinsert_iterator;
    }
    triangles_to_check.clear();
  }
  triangles_set_final_ = triangles_set;
}
