// "Copyright 2019 Billy Boys"
#include "region.h"

#include <cmath>

#include "error_strings.h"


void Swap(int xp, int yp, float array[][2]) {
  float var0 = array[xp][0];
  float var1 = array[xp][1];
  array[xp][0] = array[yp][0];
  array[xp][1] = array[yp][1];
  array[yp][0] = var0;
  array[yp][1] = var1;
}

void Region::SetNumPlateBelong(int num) {
  if (num < 0)
    throw std::invalid_argument(region_SetNumPlateBelong_ERROR);
  num_plate_belong_ = num;
}

void SelectionSort(float arr[][2], int n) {
  for (int i = 0; i < n-1; i++) {
    int min_idx = i;
    for (int j = i + 1; j < n; j++) {
      if (arr[j][0] < arr[min_idx][0]) {
        min_idx = j;
      }
    }
    Swap(min_idx, i, arr);
  }
}


glm::vec3 CenterIPoints(IntersectPoint **tabIPoints, int nbIPoints) {
  double meanX = 0.0, meanY = 0.0, meanZ = 0.0;
  for (int i = 0; i < nbIPoints; i++) {
    meanX += tabIPoints[i]->x();
    meanY += tabIPoints[i]->y();
    meanZ += tabIPoints[i]->z();
  }
  meanX /= nbIPoints;
  meanY /= nbIPoints;
  meanZ /= nbIPoints;

  return glm::dvec3(meanX, meanY, meanZ);
}


float GetAngle(glm::dvec3 u, glm::dvec3 v, glm::dvec3 center) {
  glm::dvec3 cross = Tools::Cross(u, v);
  double dotprod = Tools::Dot(u, v);

  double angle = atan2(Tools::GetNorm(cross), dotprod);

  glm::dvec3 up = Tools::Vector3DNormalize(center, 1);
  double test = Tools::Dot(up, cross);
  if (test < 0.0f) {
    angle = -angle;
  }
  angle = angle * 180.0f/M_PI;
  if (angle < 0) {
    angle += 360;
  }
  return angle;
}


void Region::SortIntersectPoints() {
  float order[number_of_ipoints_ - 1][2];

  glm::vec3 center = CenterIPoints(i_points_, number_of_ipoints_);
  glm::vec3 u = glm::vec3(i_points_[0]->x(),
                          i_points_[0]->y(),
                          i_points_[0]->z()) - center;

  for (int i = 1; i < number_of_ipoints_; i++) {
    glm::vec3 v = glm::vec3(i_points_[i]->x(),
                            i_points_[i]->y(),
                            i_points_[i]->z()) - center;
    order[i - 1][0] = GetAngle(u, v, center);
    order[i - 1][1] = i;
  }

  SelectionSort(order, number_of_ipoints_ - 1);
  IntersectPoint *copy[number_of_ipoints_];
  copy[0] = i_points_[0];

  for (int i = 1; i < number_of_ipoints_; i++) {
    copy[i] = i_points_[static_cast<int>(order[i - 1][1])];
  }

  for (int i = 0; i < number_of_ipoints_; i++) {
    i_points_[i] = copy[i];
  }
}


void Region::SetTypeRegion(float sea_level) {
  double alt = generator_->GetAltitude();
  float sea = sea_level * 2 - 1;
  float tmp = 1 - sea;
  if (alt <= sea) {
    landscape_type_ = Landscape::kOcean;
  } else if (alt < sea + 0.03f * tmp) {
    landscape_type_ = Landscape::kCoast;
  } else if (alt < sea + 0.10f * tmp) {
    landscape_type_ = Landscape::kLand;
  } else if (alt < sea + 0.28f * tmp) {
    landscape_type_ = Landscape::kHill;
  } else {
    landscape_type_ = Landscape::kMountain;
  }
}


Region::Region(Generator *generator) {
  generator_ = generator;
  number_of_ipoints_ = generator->GetNumberBelongingTriangles();
  i_points_ = new IntersectPoint *[number_of_ipoints_];
  id_ = generator->GetId();
  num_plate_belong_ = NO_PLATE;
  landscape_type_ = kOcean;
  color_ = glm::vec3(0.0, 0.0, 1.0);
  int index = 0;
  for (auto *triangle : generator->GetBelongingTriangles()) {
    i_points_[index] = new IntersectPoint(triangle->GetCircumcenter());
    index++;
  }
  SortIntersectPoints();
}


Region::~Region() {
  for (int i = 0; i < number_of_ipoints_; i++) {
    delete i_points_[i];
  }
  delete[] i_points_;
}
