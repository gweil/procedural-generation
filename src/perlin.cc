// "Copyright 2019 Billy Boys"
#include "perlin.h"

#include <cmath>
#include <ctime>

#include "tools.h"


Perlin::Perlin() {
  nb_octaves_ = DEFAULT_NB_OCTAVES;
  ComputeNoiseGrid();
}


Perlin::~Perlin() {
  DeleteGrid();
}


void Perlin::ComputeNoiseGrid() {
  std::srand(std::time(0));
  noise_grid_ = new glm::vec2 **[nb_octaves_];
  for (int i = 0; i < nb_octaves_; ++i) {
    int nb_values = pow(2, i + 1) + 2;
    noise_grid_[i] = new glm::vec2 *[nb_values];
    for (int x = 0; x < nb_values; ++x) {
      noise_grid_[i][x] = new glm::vec2[nb_values];
      for (int y = 0; y < nb_values; ++y) {
        noise_grid_[i][x][y].x = static_cast<float>(std::rand());
        noise_grid_[i][x][y].x /= RAND_MAX * 2.0 - 1;
        noise_grid_[i][x][y].y = static_cast<float>(std::rand());
        noise_grid_[i][x][y].y /= RAND_MAX * 2.0 - 1;
        noise_grid_[i][x][y] = Tools::Vector2DNormalize(noise_grid_[i][x][y],
                                                        1.0f);
      }
    }
  }
}


void Perlin::DecrementOctaves() {
  DeleteGrid();
  nb_octaves_ = std::max(nb_octaves_ - 1, 0);
  ComputeNoiseGrid();
}


void Perlin::IncrementOctaves() {
  DeleteGrid();
  nb_octaves_++;
  ComputeNoiseGrid();
}


void Perlin::SetNumberOctaves(int nb_octaves) {
  DeleteGrid();
  nb_octaves_ = nb_octaves;
  ComputeNoiseGrid();
}


float Perlin::ComputePerlinNoise(float x, float y, float z) {
  float value = 0.0f;
  for (int i = 0; i < nb_octaves_; ++i) {
    int nb_values = pow(2, i + 1) + 2;

    // UV mapping = (u, v) -> [0;1]
    float u = 0.5f + atan2(z, x) / (2 * M_PI);
    float v = 0.5 - asin(y) / M_PI;
    u *= nb_values - 1;
    v *= nb_values - 1;

    // Determine grid cell coordinates
    int u0 = static_cast<int>(u);
    int u1 = u0 + 1;
    int v0 = static_cast<int>(v);
    int v1 = v0 + 1;

    // Determine interpolation weights
    float su = u - static_cast<float>(u0);
    float sv = v - static_cast<float>(v0);

    // Interpolate between grid point gradients
    float tmp0, tmp1, alpha, beta;

    tmp0 = DotGridGradient(u0, v0, u, v, i);
    tmp1 = DotGridGradient(u1, v0, u, v, i);
    alpha = Lerp(tmp0, tmp1, su);
    tmp0 = DotGridGradient(u0, v1, u, v, i);
    tmp1 = DotGridGradient(u1, v1, u, v, i);
    beta = Lerp(tmp0, tmp1, su);
    value += Lerp(alpha, beta, sv);
  }
  return value;
}


void Perlin::DeleteGrid() {
  for (int i = 0; i < nb_octaves_; ++i) {
    int nb_values = pow(2, i + 1) + 2;
    for (int x = 0; x < nb_values; ++x) {
      delete[] noise_grid_[i][x];
    }
    delete[] noise_grid_[i];
  }
  delete[] noise_grid_;
}


// Computes the dot product of the distance and gradient vectors.
float Perlin::DotGridGradient(int iu, int iv, float u, float v, int octave) {
  glm::vec2 uv = glm::vec2(u, v);
  glm::vec2 iuv = glm::vec2(iu, iv);
  uv -= iuv;
  return Tools::Dot(uv, noise_grid_[octave][iu][iv]);
}


float Perlin::Lerp(float a0, float a1, float w) {
  return (1.0f - w) * a0 + w * a1;
}

int Perlin::GetOctaves() {
  return nb_octaves_;
}
