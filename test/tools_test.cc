// "Copyright 2019 Billy Boys"
#include <gtest/gtest.h>

#include <string>

#include "voronoi.h"


namespace {
glm::vec3 vector1(0.0, 0.0, 1.0);
glm::vec3 vector2(0.0, 1.0, 0.0);
glm::vec3 vector3(2.0, 0.0, 0.0);

TEST(Tools, IsNumberSuccess) {
  char test_true[1] = {'8'};
  ASSERT_TRUE(Tools::IsNumber(test_true));
}

TEST(Tools, IsNumberFail) {
  char test_false[1] = {'c'};
  ASSERT_FALSE(Tools::IsNumber(test_false));
}

TEST(Tools, GetNorm) {
    ASSERT_TRUE(
      (Tools::GetNorm(vector1)
      == Tools::GetNorm(vector2))
      == 1.0);

    ASSERT_TRUE(
      (Tools::GetNorm(vector3) == 2.0)
      && !(Tools::GetNorm(vector3) == Tools::GetNorm(vector2)));
}

TEST(Tools, Vector3DNormalize) {
  ASSERT_TRUE(
    Tools::Vector3DNormalize(vector1, 1.0) == vector1);

  glm::vec3 normVector = Tools::Vector3DNormalize(vector1, 3.0);
  ASSERT_TRUE(
    (normVector[0] == 0.0)
    && (normVector[1] == 0.0)
    && (normVector[2] == 3.0));
}

TEST(Tools, Dot) {
  ASSERT_TRUE(
    (Tools::Dot(vector1, vector2) == Tools::Dot(vector1, vector3))
    && (Tools::Dot(vector1, vector3) == Tools::Dot(vector2, vector3)));

  ASSERT_TRUE(
    Tools::Dot(vector1, vector2) == 0.0
    && Tools::Dot(vector1, vector1) == 1.0
    && Tools::Dot(vector3, vector3) == 4.0);
}

TEST(Tools, Cross) {
  glm::vec3 expected1X2(-1.0, 0.0, 0.0);
  ASSERT_TRUE(
    Tools::Cross(vector1, vector2) == expected1X2
  );

  glm::vec3 expected1X3(0.0, 2.0, 0.0);
  ASSERT_TRUE(
    Tools::Cross(vector1, vector3) == expected1X3
  );

  glm::vec3 expected1X1(0.0, 0.0, 0.0);
  ASSERT_TRUE(
    Tools::Cross(vector1, vector1) == expected1X1
  );
}
}  // namespace

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
