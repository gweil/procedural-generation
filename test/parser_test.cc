// "Copyright 2019 Billy Boys"
#include <gtest/gtest.h>

#include "parser.h"
#include "voronoi.h"


int nb_plates = 1;
Voronoi voro("../generatorfiles/test_files/test_parser(1).txt", nb_plates);

namespace {

// In the following tests we don't use the altitude 
// because it's generated randomly with Perlin

TEST(Parser, ImportGenerators) {
  Generator **voro_array = voro.GetDelaunay()->GetGeneratorArray();
  ASSERT_TRUE(
      (voro_array[0]->GetTheta() == 0.197395) &&
      (voro_array[0]->GetPhi() == 1.570594) &&
      (voro_array[1]->GetTheta() == 1.910633) &&
      (voro_array[1]->GetPhi() == 1.047198) &&
      (voro_array[2]->GetTheta() == 1.910633) &&
      (voro_array[2]->GetPhi() == -1.047198) &&
      (voro_array[3]->GetTheta() == -1.910633) &&
      (voro_array[3]->GetPhi() == 0.0));
  // Assert giving wrong order of keys result in error
  ASSERT_ANY_THROW(
      Voronoi v_error("../generatorfiles/test_files/test_parser(2).txt",
                      nb_plates));
}

TEST(Parser, ExportGenerators) {
  Parser::ExportGenerators("export_test.txt",
                           voro.GetDelaunay()->GetGeneratorArray(),
                           voro.GetNumberGenerator());
  Voronoi voro_test("export_test.txt", nb_plates);
  Generator **voro_array = voro.GetDelaunay()->GetGeneratorArray();
  Generator **voro_test_array = voro_test.GetDelaunay()->GetGeneratorArray();
  for (int nb_gen = 0; nb_gen < voro.GetNumberGenerator(); ++nb_gen) {
    Generator *g1 = voro_array[nb_gen];
    Generator *g2 = voro_test_array[nb_gen];
    ASSERT_TRUE(g1->GetTheta() == g2->GetTheta() &&
                g1->GetPhi() == g2->GetPhi());
  }
}
}  // namespace

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
