// "Copyright 2019 Billy Boys"
#include <gtest/gtest.h>

#include "voronoi.h"


namespace {

int nb_plates = 10;
Voronoi voro("../generatorfiles/test_files/test_500.txt",nb_plates);
TectonicPlates *tc = voro.GetTectonicPlates();

TEST(TechtonicPlates, CreationOfPlates) {
  for (int index_g = 0; index_g < voro.GetNumberGenerator(); index_g ++) {
      Region * region = voro.GetRegionManager()->GetRegion(index_g);
      bool each_region_has_plate = region->GetNumPlate() != NO_PLATE;
      ASSERT_TRUE(each_region_has_plate);

      if (!tc->RegionIsBorder(index_g)) {
        for(auto index_nr : *region->GetNeigborsRegions()) {
          Region * neighbor_region = voro.GetRegionManager()->GetRegion(index_nr);
          bool region_and_neighbor_has_same_plate = region->GetNumPlate() ==
                                               neighbor_region->GetNumPlate();
          ASSERT_TRUE(region_and_neighbor_has_same_plate);
        }
      }
  }
}


TEST(TechtonicPlates, EncapsulationPlate) {
  std::unordered_set<int> regions_of_plate = tc->GetRegionsOfPlate(0);
  regions_of_plate.insert(666);

  bool correct_encapsulation_plate = ((tc->GetRegionsOfPlate(0).find(666) ==
                              tc->GetRegionsOfPlate(0).end()) &&
                              (regions_of_plate.find(666) !=
                              regions_of_plate.end()));
  ASSERT_TRUE(correct_encapsulation_plate);

  std::vector<std::pair<IntersectPoint*,
                        IntersectPoint*>>
                        border_points = tc->GetBorderPoints();


  IntersectPoint ip = IntersectPoint(glm::vec3(4,5,6));
  IntersectPoint ip2 = IntersectPoint(glm::vec3(7,8,9));
  std::pair <IntersectPoint*, IntersectPoint*> pair_ip (&ip, &ip2);
  border_points.insert(border_points.begin(),pair_ip);


  std::vector<std::pair<IntersectPoint*, IntersectPoint*>>::iterator it1 =
                        std::find(tc->GetBorderPoints().begin(),
                                  tc->GetBorderPoints().end(),
                                  pair_ip);

  std::vector<std::pair<IntersectPoint*, IntersectPoint*>>::iterator it2 =
                        std::find(border_points.begin(),
                                  border_points.end(),
                                  pair_ip);


  bool correct_encapsulation_ip = ((it1 == tc->GetBorderPoints().end()) &&
                                   (it2 != border_points.end()));
  ASSERT_TRUE(correct_encapsulation_ip);
}
}  // namespace

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
