// "Copyright 2019 Billy Boys"
#include <gtest/gtest.h>

#include <unordered_set>

#include "generator.h"
#include "triangle.h"


double theta = 0.0f;
double phi = 1.0f;
double alt = 0.0f;
Generator g(theta, phi, alt, 0);
Generator g1(theta, phi, alt, 1);
Generator g2(2.0f, 4.0f, alt, 2);

namespace {
TEST(Generator, SameGenerator){
  // Asserts the equality of two generators
  ASSERT_TRUE(
    (g==g)
    && (g==g1)
    && !(g==g2));
}
} // namespace

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
