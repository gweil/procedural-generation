// "Copyright 2019 Billy Boys"
#include <gtest/gtest.h>

#include "region.h"
#include "voronoi.h"


namespace {

int nb_plates = 2;
Voronoi voro("../generatorfiles/test_files/test_500.txt",nb_plates);

TEST(Region, CreationOfRegion) {
    ASSERT_EQ(voro.GetNumberGenerator(),
              voro.GetRegionManager()->GetNumberRegion());

    bool equality_generatorId_RegionId;
    bool correct_neighbor_regions;
    bool correct_number_ipoints;

    for (int index_g = 0; index_g < voro.GetNumberGenerator(); index_g ++) {
      Region * region = voro.GetRegionManager()->GetRegion(index_g);
      equality_generatorId_RegionId = (unsigned int)index_g == region->GetId();
      ASSERT_TRUE(equality_generatorId_RegionId);

      for(auto index_nr : *region->GetNeigborsRegions()) {
        Region * neighbor_region = voro.GetRegionManager()->GetRegion(index_nr);
        correct_neighbor_regions = neighbor_region->GetNeigborsRegions()->find(index_g) !=
                                   neighbor_region->GetNeigborsRegions()->end();
        ASSERT_TRUE(correct_neighbor_regions);
      }
      correct_number_ipoints = (unsigned int)region->GetNumberOfIPoints() ==
                               region->GetNeigborsRegions()->size();
      ASSERT_TRUE(correct_number_ipoints);
    }
}


}  // namespace

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
