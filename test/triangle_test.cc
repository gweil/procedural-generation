// "Copyright 2019 Billy Boys"
#include <gtest/gtest.h>

#include "generator.h"


namespace {

Generator v0(0.0f, 0.0f, 0.0f, 0);
Generator v1(1.0f, 0.0f, 0.0f, 1);
Generator v2(0.0f, 1.0f, 0.0f, 2);
Generator v3(1.0f, 1.0f, 1.0f, 3);
Generator v4(glm::vec3(-0.79, 0.15, 0.59), 1.0f, 4);
Generator v5(glm::vec3(-0.36, -0.8, 0.48), 1.0f, 5);
Generator v6(glm::vec3(-0.81, -0.4, -0.43), 1.0f, 3);
Triangle t(&v0,  &v1, &v2);
Triangle t2(&v1, &v0, &v2);
Triangle t3(&v0, &v1, &v3);
Triangle t4(&v4, &v5, &v6);

TEST(Triangle, DefaultConstructor) {
  // Asserts the c'tor to fail with invalid generator values.
  ASSERT_ANY_THROW(
    Triangle t_error(&v0, &v0, &v0));
  ASSERT_ANY_THROW(
    Triangle t_error(&v1, &v0, &v0));
  ASSERT_ANY_THROW(
    Triangle t_error(&v0, &v1, &v0));
  ASSERT_ANY_THROW(
    Triangle t_error(&v0, &v0, &v1));
}


TEST(Triangle, CorrectVertices) {
  // Asserts vertices values corresponds to the values given in the c'tor.
  Generator **vertices = t.GetVertices();
  ASSERT_TRUE(
    (*vertices[0] == v0)
    && (*vertices[1] == v1)
    && (*vertices[2] == v2));

  // Asserts vertices are different from one another.
  ASSERT_TRUE(
    !(vertices[0] == vertices[1])
    && !(vertices[0] == vertices[2])
    && !(vertices[1] == vertices[2]));
}


TEST(Triangle, InCircumscribedSphere) {
  ASSERT_TRUE(
    !(t4.InCircumscribedSphere(&v4))
    && !(t4.InCircumscribedSphere(&v5))
    && !(t4.InCircumscribedSphere(&v6)));
}


TEST(Triangle, IsGeneratorInTriangle) {
  // Asserts the presence of generators in triangle
  ASSERT_TRUE(
    (t.IsGeneratorInTriangle(&v0))
    && (t.IsGeneratorInTriangle(&v1))
    && (t.IsGeneratorInTriangle(&v2))
    && !(t.IsGeneratorInTriangle(&v3)));
}


TEST(Triangle, SameVertices) {
  ASSERT_TRUE(
    (t.SameVertices(t))
    && (t.SameVertices(t2))
    && !(t.SameVertices(t3)));
}

TEST(Triangle, CircumcenterValue) {
  glm::vec3 circumT3 = t3.GetCircumcenter();
  glm::vec3 circumT4 = t4.GetCircumcenter();

  ASSERT_TRUE(
    (circumT3[0] > 0.39 && circumT3[0] < 0.40)
    && (circumT3[1] > 0.21 && circumT3[1] < 0.22)
    && (circumT3[2] > 0.72 && circumT3[2] < 0.73)
    && (circumT4[0] > -0.68 && circumT4[0] < -0.67)
    && (circumT4[1] > -0.33 && circumT4[1] < -0.32)
    && (circumT4[2] > 0.18 && circumT4[2] < 0.19));
}


TEST(Triangle, SameTriangle) {
  // Asserts the equality of two triangles
  ASSERT_TRUE(
    (t == t)
    && (t == t2)
    && !(t == t3));
}


TEST(Triangle, SetManagement) {
  Generator v4(3.0f, 1.0f, 2.0f, 4);
  Triangle *t4 = new Triangle(&v0, &v3, &v4);
   // Asserts the correct adding of triangles in triangle set
   // and the absence of two identicals triangles : i.e same vertices
  ASSERT_TRUE(
    (v0.GetBelongingTriangles().size() == 3)
    && (v1.GetBelongingTriangles().size() == 2)
    && (v2.GetBelongingTriangles().size() == 1)
    && (v3.GetBelongingTriangles().size() == 2)
    && (v4.GetBelongingTriangles().size() == 1));

  // Asserts the presence of triangles or equivalent triangles in set
  ASSERT_TRUE(
    (v0.GetBelongingTriangles().find(&t) != v0.GetBelongingTriangles().end())
    && (v0.GetBelongingTriangles().find(&t2) != v0.GetBelongingTriangles().end())
    && (v3.GetBelongingTriangles().find(&t) == v3.GetBelongingTriangles().end()));


  // Asserts the actualisation of the set of Triangle in generators
  // after the delete of one of them
  delete (t4);
  t4 = nullptr;

  ASSERT_TRUE(
    (v0.GetBelongingTriangles().size() == 2)
    && (v1.GetBelongingTriangles().size() == 2)
    && (v3.GetBelongingTriangles().size() == 1)
    && (v4.GetBelongingTriangles().size() == 0)
  );
}


}  // namespace

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
