// "Copyright 2019 Billy Boys"
#include <gtest/gtest.h>

#include "voronoi.h"


int nb_plates = 1;
Voronoi voro("../generatorfiles/exportGenerator_500.txt", nb_plates);

namespace {
TEST(Perlin, CorrectValues) {
  int nb_generators = voro.GetNumberGenerator();
  for (int count = 0; count < nb_generators; ++count) {
    double alt = voro.GetDelaunay()->GetGeneratorArray()[count]->GetAltitude();
    ASSERT_LE(alt, 1.0);
    ASSERT_GE(alt, -1.0);
  }
}
}  // namespace

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
