// "Copyright 2019 Billy Boys"
#include <gtest/gtest.h>

#include "voronoi.h"


namespace {

int nb_plates = 1;
Voronoi voro("../generatorfiles/test_files/test_parser(1).txt",nb_plates);

TEST(RegionManager, WaterLevel) {
  bool correct_waterlevelincrement;
  bool correct_waterleveldecrement;

  float base_sealevel = voro.GetRegionManager()->GetSeaLevel();
  voro.GetRegionManager()->IncWaterLevel();
  voro.GetRegionManager()->IncWaterLevel();
  correct_waterlevelincrement = GAP_SEA_LEVEL * 2 + base_sealevel ==
                                voro.GetRegionManager()->GetSeaLevel();
  ASSERT_TRUE(correct_waterlevelincrement);

  base_sealevel = voro.GetRegionManager()->GetSeaLevel();
  voro.GetRegionManager()->DecWaterLevel();
  voro.GetRegionManager()->DecWaterLevel();
  correct_waterleveldecrement = -GAP_SEA_LEVEL * 2 + base_sealevel ==
                                voro.GetRegionManager()->GetSeaLevel();
  ASSERT_TRUE(correct_waterleveldecrement);
}


TEST(RegionManager, Temperature) {
  bool correct_temperatureincrement;
  bool correct_Temperaturedecrement;

  voro.GetRegionManager()->IncTemperature();
  voro.GetRegionManager()->IncTemperature();

  correct_temperatureincrement = voro.GetRegionManager()->GetTemperature() ==
                                 kTemperate + 2;
  ASSERT_TRUE(correct_temperatureincrement);

  voro.GetRegionManager()->DecTemperature();
  voro.GetRegionManager()->DecTemperature();

  correct_Temperaturedecrement = voro.GetRegionManager()->GetTemperature() ==
                                 kHottest - 2;
  ASSERT_TRUE(correct_Temperaturedecrement);
}

TEST(RegionManager, Moisture) {
  bool correct_moistureincrement;
  bool correct_moisturedecrement;

  voro.GetRegionManager()->DecMoisture();
  voro.GetRegionManager()->DecMoisture();
  voro.GetRegionManager()->DecMoisture();
  correct_moisturedecrement = voro.GetRegionManager()->GetMoisture() ==
                                 kNormal - 2;
  ASSERT_TRUE(correct_moisturedecrement);

  voro.GetRegionManager()->IncMoisture();
  voro.GetRegionManager()->IncMoisture();
  correct_moistureincrement = voro.GetRegionManager()->GetMoisture() ==
                                 kNormal;
  ASSERT_TRUE(correct_moistureincrement);
}
}  // namespace
int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
