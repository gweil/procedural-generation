// "Copyright 2019 Billy Boys"
#include <gtest/gtest.h>

#include <cmath>

#include "glm/vec3.hpp"
#include "controler.h"
#include "region.h"


int nb_plates = 1;
Controler controler(10, nb_plates);
int nb_generator = controler.GetNumberGenerator();

namespace {
TEST(Controler, EventHandling) {
  glm::vec3 colors_memory[nb_generator];
  for (int counter = 0; counter < nb_generator; ++counter)
    colors_memory[counter] = controler.GetRegion(counter)->GetColor();
  unsigned char event_dec_temp[3] = {2, 2, 2};
  unsigned char event_inc_temp[3] = {5, 5, 5};
  unsigned char event_dec_moisture[3] = {8, 8, 8};
  unsigned char event_inc_moisture[3] = {10, 10, 10};
  unsigned char event_dec_water[3] = {15, 15, 15};
  unsigned char event_inc_water[3] = {13, 13, 13};
  // Change temperature
  controler.EventOnPress(event_dec_temp);
  controler.EventOnPress(event_inc_temp);
  for (int counter = 0; counter < nb_generator; ++counter)
    ASSERT_EQ(colors_memory[counter],
              controler.GetRegion(counter)->GetColor());
  // Change moisture
  controler.EventOnPress(event_dec_moisture);
  controler.EventOnPress(event_inc_moisture);
  for (int counter = 0; counter < nb_generator; ++counter)
    ASSERT_EQ(colors_memory[counter],
              controler.GetRegion(counter)->GetColor());
  // Change water level
  controler.EventOnPress(event_dec_water);
  controler.EventOnPress(event_inc_water);
  for (int counter = 0; counter < nb_generator; ++counter)
    ASSERT_EQ(colors_memory[counter],
              controler.GetRegion(counter)->GetColor());
}

TEST(Controler, CoordIPoints) {
  glm::vec3 vec = controler.CoordIPoint(nb_generator - 1, 1000);
  ASSERT_TRUE(std::isnan(vec.x));
  vec = controler.CoordIPoint(nb_generator, 0);
  ASSERT_TRUE(std::isnan(vec.x));
  vec = controler.CoordIPoint(nb_generator - 1, 0);
  EXPECT_FALSE(std::isnan(vec.x));
}
}  // namespace

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
