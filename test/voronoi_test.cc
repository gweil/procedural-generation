// "Copyright 2019 Billy Boys"
#include <gtest/gtest.h>

#include "voronoi.h"


namespace {
int nb_plates = 1;
Voronoi voro_correct("../generatorfiles/test_files/test_parser(1).txt",nb_plates);
Voronoi voro_incorrect("../generatorfiles/test_files/voronoi_incorrect__construction_1000.txt",nb_plates);

TEST(Voronoi, VerifyVoronoiDiagram) {
  ASSERT_TRUE(voro_correct.VerifyVoronoiDiagram());
  ASSERT_TRUE(!voro_incorrect.VerifyVoronoiDiagram());
}

}  // namespace




int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
